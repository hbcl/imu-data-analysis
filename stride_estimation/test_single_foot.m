% A test of the single-foot data processing as a whole.

% This test is not (yet) written for the Matlab testing framework.

% Parameters
stationary_period_params = StationaryPeriodParams();
foot_fall_params = FootFallDetectionParams();
tilt_correction_params = TiltCorrectionKalmanFilterParams();
step_filter_params = StepFilterParams();
plot_params = PlotParams();

% Load IMU information from a file
[Wb, Ab, sampling_period] = ...
  getdata_ms('test_data/data_left_memsense.dat');

% Define the section (in seconds) that will need to be processed and segment the IMU data accordingly
SECTION = [190, 280];
[Wb,Ab] = getdata(Wb, Ab, sampling_period, SECTION);

% Perform inertial mechanization
walk_info = integrate_single_foot_imu( ...
  Wb, Ab, sampling_period, stationary_period_params, foot_fall_params, tilt_correction_params);

% Segment strides only for the walking period
strides = stride_segmentation(walk_info, sampling_period, step_filter_params);

% Plot results
plt_ltrl_frwd_strides(strides, plot_params);
plt_frwd_elev_strides(strides, plot_params);
plt_stride_var(strides, plot_params);

% Test: Check the covariance of the stride displacements against a reference
steps_forwardDistance = strides.frwd(end, :)';
steps_lateralDistance = strides.ltrl(end, :)';

mean_steps_forwardDistance = mean(steps_forwardDistance);
mean_steps_lateralDistance = mean(steps_lateralDistance);

steps_mean = [ mean_steps_forwardDistance, mean_steps_lateralDistance ];
steps_covariance = cov([steps_forwardDistance - mean_steps_forwardDistance, ...
                        steps_lateralDistance - mean_steps_lateralDistance ]);

target_steps_mean = [ 1.217446670107096  0.023077092393984 ];
target_steps_covariance = [ ...
       0.344138631388191  -6.174030710478229e-04 ; ...
  -6.174030710478229e-04      0.005046508155774 ];

diff_steps_mean = sqrt(sum((steps_mean - target_steps_mean) .^ 2));
diff_steps_covariance = ...
  sqrt(sum((steps_covariance - target_steps_covariance) .^ 2, ...
           'all') ...
      );

% This test is intended to detect unintentional changes to the processing
% pipeline. Therefore, we aim to initially underestimate the tolerances, then
% increase them if they produce false warnings.
diff_steps_mean_tolerance       = 1e-6;
diff_steps_covariance_tolerance = 1e-6;

if diff_steps_mean > diff_steps_mean_tolerance
  error(sprintf( ...
    '***Test failed***!\nThe norm of the difference of the steps mean and the reference mean (%f) exceeds the tolerance (%f).', ...
    diff_steps_mean, ...
    diff_steps_mean_tolerance ...
    ));
end

if diff_steps_covariance > diff_steps_covariance_tolerance
  error(sprintf( ...
    '***Test failed***!\nThe Frobenius norm of the difference of the steps covariance and the reference covariance (%f) exceeds the tolerance (%f).', ...
    diff_steps_covariance, ...
    diff_steps_covariance_tolerance ...
    ));
end

display('***Test passed.***');
display(sprintf( ...
  'The norm of the difference of the steps mean and the reference mean (%f) is below the tolerance (%f).', ...
  diff_steps_mean, ...
  diff_steps_mean_tolerance ...
  ));
display(sprintf(...
  'The Frobenius norm of the difference of the steps covariance and the reference covariance (%f) is below the tolerance (%f).', ...');
  diff_steps_covariance, ...
  diff_steps_covariance_tolerance ...
  ));
