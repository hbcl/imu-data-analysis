% Author: Lauro Ojeda, 2011-2015
function [stride, number_of_steps] = filter_steps(stride, number_of_steps, step_filter_params)

% Unpack parameters
MAX_STEP_LENGTH = step_filter_params.post_filter_max_step_length;
MIN_STEP_LENGTH = step_filter_params.post_filter_min_step_length;
MAX_STDEV = step_filter_params.post_filter_max_stdevs;
FILTER_ELEVATION = step_filter_params.post_filter_do_filter_elevation;

% Filter out steps with forward displacement outside of the specified bounds
frwd = stride.frwd';
outlier_indices = ...
  find(step_filter_params.post_filter_max_step_length < frwd(:, end));
if(~isempty(outlier_indices))
	disp('_frwd LONG');
	[stride, number_of_steps] = cut_step_section(stride, outlier_indices, number_of_steps);
end;

frwd = stride.frwd';
outlier_indices = find(frwd(:, end) < step_filter_params.post_filter_min_step_length);
if(~isempty(outlier_indices))
	disp('_frwd SHORT');
    disp(outlier_indices);
	[stride, number_of_steps] = cut_step_section(stride, outlier_indices, number_of_steps);
end;

% Filter out steps with extreme forward displacement, as defined by the median
% and standard deviation of the forward displacement of the remaining steps.
frwd = stride.frwd';
median_pos_frwd = median(frwd(:, end));
std_pos_frwd = std(frwd(:, end));
outlier = find(abs(frwd(:, end) - median_pos_frwd) > std_pos_frwd*MAX_STDEV);
if(~isempty(outlier))
	disp('_frwd +2 STDEV');
	[stride, number_of_steps] = cut_step_section(stride, outlier, number_of_steps);
end;

% Filter out steps with extreme lateral displacement, as defined by the
% standard deviation of the lateral displacement of the remaining steps. The
% mean/median is assumed to be zero.
ltrl = stride.ltrl';
std_pos_ltrl = std(ltrl(:, end));
outlier = find(abs(ltrl(:, end)) > std_pos_ltrl*MAX_STDEV);
if(~isempty(outlier))
	disp('_ltrl +2 STDEV');
	[stride, number_of_steps] = cut_step_section(stride, outlier, number_of_steps);
end;

% Filter out steps with extreme vertical displacement, as defined by the
% median and standard deviation of the vertical displacement of the remaining
% steps.
if(FILTER_ELEVATION)
  elev = stride.elev';
  median_pos_elev = median(elev(:, end));
  std_pos_elev = std(elev(:, end));
  outlier = find(abs(elev(:, end) - median_pos_elev) > std_pos_elev*MAX_STDEV);
  if(~isempty(outlier))
      disp('_elev +2 STDEV');
      [stride, number_of_steps] = cut_step_section(stride, outlier, number_of_steps);
  end;
end;
