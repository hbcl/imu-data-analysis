classdef IMUAnalysisParameters
  % A collection of all the parameter classes

  properties
    foot_fall_detection_params; % FootFallDetectionParams
    foot_fall_opposite_velocity_detection_params; % FootFallOppositeVelocityDetectionParams
    plot_params; % PlotParams
    stationary_period_params; % StationaryPeriodParams
    step_filter_params; % StepFilterParams
    tilt_correction_kalman_filter_params; % TiltCorrectionKalmanFilterParams
  end

  methods
    % Default values
    function obj = IMUAnalysisParameters(obj)
      obj.foot_fall_detection_params = FootFallDetectionParams();
      obj.foot_fall_opposite_velocity_detection_params = FootFallOppositeVelocityDetectionParams();
      obj.plot_params = PlotParams();
      obj.stationary_period_params = StationaryPeriodParams();
      obj.step_filter_params = StepFilterParams();
      obj.tilt_correction_kalman_filter_params = TiltCorrectionKalmanFilterParams();
    end
  end
end
