% Author: Lauro Ojeda, 2012-2015
function [LeftWb,LeftAb,RightWb,RightAb,PERIOD,LeftMb,RightMb] = ...
    sync_New_apdm(L_FILE,R_FILE,SYNC,FORCE_SYNC_VALUE)

    if(~exist('SYNC','var'))
        SYNC = 1; 
    end;
    if(~exist('FORCE_SYNC_VALUE','var'))
        FORCE_SYNC_VALUE = 0;% Use a non-zero value (counts) combined with SYNC=1 in order to force data synchronization
    end;

    % Read files
    L_time_stamps = read_times(L_FILE);
    R_time_stamps = read_times(R_FILE);
    
    FREQ = read_matching_frequencies(L_FILE, R_FILE); % Sanity check
    PERIOD = 1/FREQ;

    % Get synchronization frame shifts
    [L_starting_index, R_starting_index] = ...
        synchronize(SYNC, FORCE_SYNC_VALUE, L_time_stamps, R_time_stamps);

    if (L_starting_index > 1)
        display(sprintf('Shift Left IMU signals [%d]', L_starting_index));
    end;
    if (R_starting_index > 1)
        display(sprintf('Shift Right IMU signals [%d]', R_starting_index));
    end;
        
    L_SECTION = [L_starting_index, size(L_time_stamps,1)];
    R_SECTION = [R_starting_index, size(R_time_stamps,1)];

    % Load (synchronized?) data
    [Wb, Ab, ~, Mb] = getdata_New_apdm(L_FILE,1);
    [LeftWb, LeftAb, LeftMb] = crop(L_SECTION, Wb, Ab, Mb);
    
    [Wb, Ab, ~, Mb] = getdata_New_apdm(R_FILE,1);
    [RightWb, RightAb, RightMb] = crop(R_SECTION, Wb, Ab, Mb);
end

%% File helpers
% These functions assume a certain file structure. Not sure how to abstract
% this.

function times = read_times(filepath)
    % Should this be renamed "read_time_stamps"?
    info = h5info(filepath);
    times = h5read(filepath, [info.Groups(2).Groups(1).Name,'/Time']);
end

function frequency = read_frequency(filepath)
    info = h5info(filepath);
    raw_frequency = h5readatt(filepath, info.Groups(2).Groups(1).Groups(1).Name, 'Sample Rate');
    frequency = double(raw_frequency);
end

function frequency = read_matching_frequencies(filepath1, filepath2)
    freq1 = read_frequency(filepath1);
    freq2 = read_frequency(filepath2);
    
    if (freq1 ~= freq2)
        ME = MException('sync_New_apdm:nonIdenticalFrequencies', ...
            'The sampling frequencies of the left file (%s, %f Hz) and right file (%s, %f Hz) do not match.', ...
            filepath1, freq1, filepath2, freq2);
        throw(ME);
    end;

    frequency = freq1;
end

%% Synchronization helpers

% Synchronize two series of timestamps by dropping inital frames of one
% file. This function returns the indices at which each series should start
% in order to synchronize them.
function [starting_index1, starting_index2] = ...
    synchronize(SYNC, FORCE_SYNC_VALUE, times1, times2)

    if (~SYNC)
        warning('Not Syncing');
        starting_index1 = 1;
        starting_index2 = 1;
    else
        if (FORCE_SYNC_VALUE)
            warning('Sync with user defined value');
            if (FORCE_SYNC_VALUE > 0)
                starting_index1 = FORCE_SYNC_VALUE;
                starting_index2 = 1;
            else
                starting_index1 = 1;
                starting_index2 = FORCE_SYNC_VALUE;
            end;
        else
            [starting_index1, starting_index2] = ...
                synchronize_by_time_stamps(times1, times2);
        end;
    end;
    
end

function [starting_index1, starting_index2] = ...
    synchronize_by_time_stamps(times1, times2)

    if(times1(1) <= times2(1))
        % The first file starts before the second
        starting_index1 = find(times1 >= times2(1),1);
        starting_index2 = 1;
    else
        % The second file starts before the first
        starting_index1 = 1;
        starting_index2 = find(times2 >= times1(1),1);
    end;
end