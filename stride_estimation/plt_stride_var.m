% Author: Lauro Ojeda, 2011-2015
function plt_stride_var(strides, plot_params, CREATE_NEW_FIGURE, DETREND_STEP_LENGTH, SHIFT_TO_ZERO)
  SHOW_NUMBER = plot_params.show_number;

  if(~exist('CREATE_NEW_FIGURE','var') | CREATE_NEW_FIGURE==1)
    figure;
  end;

  if(~exist('SHIFT_TO_ZERO','var'))
    SHIFT_TO_ZERO = 1;
  end;

  hold on;

    if (~exist('DETREND_STEP_LENGTH','var') | DETREND_STEP_LENGTH == 0)
      % Use full data
      frwd = strides.frwd(end, :);
    else
      % Use de-trended data
      frwd = strides.frwd_speed_compensated;
    end;

    ltrl = strides.ltrl(end, :);
    [cx, cy, ex, ey, covar] = compute_cov(ltrl, frwd, SHIFT_TO_ZERO);

    n_steps = size(ltrl, 2);
    colors = jet(n_steps);

    for step_no = 1:n_steps
      plot( cx(step_no), cy(step_no), 'o', ...
            'MarkerEdgeColor', 'k', ...
            'MarkerFaceColor', colors(step_no, :), ...
            'MarkerSize', 5);

      if SHOW_NUMBER
        text(cx(step_no)+0.001, cy(step_no), num2str(step_no));
      end;
    end;

    plot( ex, ey, ...
          'Color', 'r', ...
          'LineWidth', 3);
    grid on;
    ylabel('Frwd [m]');
    xlabel('Ltrl [m]');
    axis equal;

  hold off;
end
