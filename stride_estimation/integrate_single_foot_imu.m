function walk_info ... % struct
  = integrate_single_foot_imu( ...
      imu_local_angular_velocities, ... % Matrix of Doubles (n_frames x 3): finite difference form (rad/s * sampling_period)
      imu_local_linear_accelerations, ... % Matrix of Doubles (n_frames x 3): m/s^2
      sampling_period, ... % Double, s: the IMU sampling period
      stationary_period_params, ... % StationaryPeriodParams
      foot_fall_params, ... % FootFallDetectionParams
      tilt_correction_params  ... % TiltCorrectionKalmanFilterParams
    )

  % Reorient the raw IMU data to a global reference frame
  [euler_angles, global_linear_accelerations] = orient_raw_imu_data( ...
    sampling_period, ...
    stationary_period_params, ...
    tilt_correction_params, ...
    imu_local_angular_velocities, ...
    imu_local_linear_accelerations ...
    );

  % Segment the data into strides by choosing the boundary indices separating
  % strides. We refer to these indices as 'foot falls'.
  foot_fall_frame_mask = foot_falls_from_raw_imu_data( ...
                            sampling_period, ...
                            stationary_period_params, ...
                            foot_fall_params, ...
                            imu_local_angular_velocities, ...
                            imu_local_linear_accelerations ...
                          );

  % Integrate the linear accelerations, controlling drift by setting the change
  % in velocity over each step to be zero.
  global_linear_accelerations_with_zvu = zero_velocity_update( ...
    foot_fall_frame_mask, global_linear_accelerations);

  [ global_linear_velocities, ...
    horizontal_linear_velocity_magnitudes, ...
    positions ] = ...
    integrate_linear_accelerations( ...
      sampling_period, ...
      global_linear_accelerations_with_zvu);

  % Pack output struct
  walk_info.euler_angles = euler_angles;
  walk_info.foot_fall_frame_mask = foot_fall_frame_mask;
  walk_info.global_linear_accelerations = global_linear_accelerations;
  walk_info.global_linear_velocities = global_linear_velocities;
  walk_info.horizontal_linear_velocity_magnitudes = horizontal_linear_velocity_magnitudes;
  walk_info.positions = positions;

  % Plot results
  n_frames = size(imu_local_angular_velocities, 1);
  times = (1:n_frames)*sampling_period;

  plot_attitude(times, walk_info.euler_angles, walk_info.foot_fall_frame_mask);
  plot_velocities( ...
      times, walk_info.global_linear_velocities, ...
      walk_info.horizontal_linear_velocity_magnitudes, ...
      walk_info.foot_fall_frame_mask);
  plot_positions(walk_info.positions, walk_info.foot_fall_frame_mask);

end % function


% Plotting functions

function plot_attitude(times, euler_angles, foot_fall_frame_mask)
  euler_degrees = euler_angles*180/pi;

  figure;
  hold on;
    title('Attitudes');

    xlabel('time [s]');
    ylabel('Euler angles [deg]');

    plot( times, ...
          euler_degrees);
    plot( times(foot_fall_frame_mask), ...
          euler_degrees(foot_fall_frame_mask,:), ...
          '*');
    legend('Roll', 'Pitch', 'Heading');

    grid on;
  hold off;
end

function plot_velocities( ...
  times, ...
  global_linear_velocities, ...
  horizontal_linear_velocity_magnitudes, ...
  foot_fall_frame_mask ...
  )

  figure;
  hold on;
    title('Velocities');

    xlabel('time [s]');
    ylabel('V [m/s]');

    plot( times, ...
          global_linear_velocities);
    plot( times, ...
          horizontal_linear_velocity_magnitudes, ...
          'k');
    plot( times(foot_fall_frame_mask), ...
          horizontal_linear_velocity_magnitudes(foot_fall_frame_mask), ...
          '*g');

    legend('Vx', 'Vy', 'Vz', '|V|', 'Footfall');

    grid on;
  hold off;
end

function plot_positions(positions, foot_fall_frame_mask)
  figure;
  hold on;
    title('Positions');
    xlabel('X [m]');
    ylabel('Y [m]');
    zlabel('Z [m]');

    plot3( positions(:, 1), ...
          -positions(:, 2), ...
          -positions(:, 3));
    plot3( positions(foot_fall_frame_mask, 1), ...
          -positions(foot_fall_frame_mask, 2), ...
          -positions(foot_fall_frame_mask, 3), ...
          '.k');

    axis equal;
    grid on;
  hold off;
end
