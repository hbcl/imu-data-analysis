% PlotStandingTrialMarkers

data = load('02.mat')
flds = fields(data);
flds = flds(1:3);
markers = structfun(@(a) a{1}(:,1:3),rmfield(data,flds),'UniformOutput',0);
flds = fields(markers);

markersMN = structfun(@(a) mean(a(400:900,:),1), markers,'UniformOutput',0);

% plot them all
figure; hold on;
structfun(@(a) plotxyz(a,'.'),markersMN);
cellfun(@(a) text(markersMN.(a)(1,1),markersMN.(a)(1,2),markersMN.(a)(1,3),[' ' a],'interpreter','none') ,flds); axis equal; axis vis3d; grid on; rotate3d; 

% 
allmarkers = struct2cell(markersMN);
allmarkers = cat(1, allmarkers{:});
hold on; scatterxyz(allmarkers,[],1:length(allmarkers)); axis equal; axis vis3d; rotate3d; 

save('Standing_Pose.mat')


