function varargout = MetricMakerLR(varargin)
% METRICMAKERLR MATLAB code for MetricMakerLR.fig
%      METRICMAKERLR, by itself, creates a new METRICMAKERLR or raises the existing
%      singleton*.
%
%      H = METRICMAKERLR returns the handle to a new METRICMAKERLR or the handle to
%      the existing singleton*.
%
%      METRICMAKERLR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in METRICMAKERLR.M with the given input arguments.
%
%      METRICMAKERLR('Property','Value',...) creates a new METRICMAKERLR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MetricMakerLR_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MetricMakerLR_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MetricMakerLR

% Last Modified by GUIDE v2.5 27-Aug-2014 11:57:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MetricMakerLR_OpeningFcn, ...
                   'gui_OutputFcn',  @MetricMakerLR_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
end

% --- Executes just before MetricMakerLR is made visible.
function MetricMakerLR_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MetricMakerLR (see VARARGIN)

% Choose default command line output for MetricMakerLR
handles.output = hObject;
handles.startdir = pwd;
handles.lookindir = pwd;
handles.imu_file_left = 0;
handles.imu_file_right = 0;
handles.imu_file_trunk = 0;
handles.imu_file_wrist = 0;

handles.imudataleft = 0;
handles.imudataright = 0;
handles.imudatatrunk = 0;
handles.imudatawrist = 0;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MetricMakerLR wait for user response (see UIRESUME)
% uiwait(handles.figure1);
end

% --- Outputs from this function are returned to the command line.
function varargout = MetricMakerLR_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
end

% --- Executes on button press in video_button.
function video_button_Callback(hObject, eventdata, handles)
% hObject    handle to video_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cd(handles.lookindir)
[handles.video_folder, handles.video_filepath]=uigetfile('*.*','Choose Video File');
handles.videofilename=[handles.video_filepath handles.video_folder];
handles.videoObj = VideoReader(handles.videofilename);
if ~isempty(handles.video_filepath)
handles.lookindir = handles.video_filepath;
end
guidata(hObject,handles);
set(hObject,'String', handles.video_folder);
cd(handles.startdir)
end

% --- Executes on button press in imu_button_left.
function imu_button_left_Callback(hObject, eventdata, handles)
% hObject    handle to imu_button_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cd(handles.lookindir)
[handles.imu_file_left, handles.imu_filepath_left] = uigetfile('*.*','Choose IMU File');
handles.imufilenameleft=[handles.imu_filepath_left   handles.imu_file_left];
if handles.imu_filepath_left ~= 0
handles.lookindir = handles.imu_filepath_left;
end
% handles.imu_filepath=which(handles.imu_folder);
if isfield(handles,'imudataleft')
handles = rmfield(handles,'imudataleft');
end
guidata(hObject,handles);
set(hObject, 'String',handles.imu_file_left);
cd(handles.startdir)
end

% --- Executes on button press in imu_button_right.
function imu_button_right_Callback(hObject, eventdata, handles)
% hObject    handle to imu_button_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cd(handles.lookindir)
[handles.imu_file_right, handles.imu_filepath_right] = uigetfile('*.*','Choose Right Foot IMU File');
handles.imufilenameright=[handles.imu_filepath_right   handles.imu_file_right];
if handles.imu_filepath_right ~= 0
handles.lookindir = handles.imu_filepath_right;
end
% handles.imu_filepath=which(handles.imu_folder);
if isfield(handles,'imudataright')
handles = rmfield(handles,'imudataright');
end
guidata(hObject,handles);
set(hObject, 'String',handles.imu_file_right);
cd(handles.startdir)
end

% --- Executes on button press in imu_button_trunk.
function imu_button_trunk_Callback(hObject, eventdata, handles)
% hObject    handle to imu_button_trunk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cd(handles.lookindir)
[handles.imu_file_trunk, handles.imu_filepath_trunk] = uigetfile('*.*','Choose Trunk IMU File');
handles.imufilenametrunk=[handles.imu_filepath_trunk   handles.imu_file_trunk];
if handles.imu_filepath_trunk ~= 0
handles.lookindir = handles.imu_filepath_trunk;
end
% handles.imu_filepath=which(handles.imu_folder);
if isfield(handles,'imudatatrunk')
handles = rmfield(handles,'imudatatrunk');
end
guidata(hObject,handles);
set(hObject, 'String',handles.imu_file_trunk);
cd(handles.startdir)
end


% --- Executes on button press in imu_button_wrist.
function imu_button_wrist_Callback(hObject, eventdata, handles)
% hObject    handle to imu_button_wrist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cd(handles.lookindir)
[handles.imu_file_wrist, handles.imu_filepath_wrist] = uigetfile('*.*','Choose Wrist IMU File');
handles.imufilenamewrist=[handles.imu_filepath_wrist   handles.imu_file_wrist];
if handles.imu_filepath_wrist ~= 0
handles.lookindir = handles.imu_filepath_wrist;
end
% handles.imu_filepath=which(handles.imu_folder);
if isfield(handles,'imudatawrist')
handles = rmfield(handles,'imudatawrist');
end
guidata(hObject,handles);
set(hObject, 'String',handles.imu_file_wrist);
cd(handles.startdir)
end


% --- Executes on button press in go.
function go_Callback(hObject, eventdata, handles)
% hObject    handle to go (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.stop=false;
% imufilename=[handles.imu_filepath handles.imu_folder];
% videofilename=[handles.video_filepath handles.video_folder];;
% videoObj = VideoReader(handles.videofilename);
% videoframerate=handles.videoObj.FrameRate;
% videonumberframes = handles.videoObj.NumberOfFrames;
% removeheadings(filename,'newfile.csv');
% imudata0 = csvread('newfile.csv');
if ~isfield(handles,{'imudataleft'}) 
    try
    [time,p,q,indsFF] = ProcessIMUWalkCSVData(handles.imufilenameleft);
    handles.imudataleft = struct('time',time,'p',p,'q',q,'indsFF',indsFF);
    handles.imudataempty = handles.imudataleft; handles.imudataempty.p = NaN*handles.imudataempty.p; handles.imudataempty.q = repmat([1 0 0 0],size(handles.imudataempty.q,1),1); handles.imudataempty.indsFF = []; handles.imudataempty.source = 'Left';
    catch
        'no left foot imu'
    end
end
if ~isfield(handles,{'imudataright'}) 
    try
    [time,p,q,indsFF] = ProcessIMUWalkCSVData(handles.imufilenameright);
    handles.imudataright = struct('time',time,'p',p,'q',q,'indsFF',indsFF);
    handles.imudataempty = handles.imudataright; handles.imudataempty.p = NaN*handles.imudataempty.p; handles.imudataempty.q = repmat([1 0 0 0],size(handles.imudataempty.q,1),1); handles.imudataempty.indsFF = []; handles.imudataempty.source = 'Right';
    catch
        'no right foot imu'
    end
end
if ~isfield(handles,{'imudatatrunk'})
    try
    [time,p,q,indsFF] = ProcessIMUWalkCSVData(handles.imufilenametrunk);
    handles.imudatatrunk = struct('time',time,'p',p,'q',q,'indsFF',indsFF);
    handles.imudataempty = handles.imudatatrunk; handles.imudataempty.p = NaN*handles.imudataempty.p; handles.imudataempty.q = repmat([1 0 0 0],size(handles.imudataempty.q,1),1); handles.imudataempty.indsFF = []; handles.imudataempty.source = 'Trunk';
    catch
        'no trunk imu'
    end
end
if ~isfield(handles,{'imudatawrist'}) 
    try
    [time,p,q,indsFF] = ProcessIMUWalkCSVData(handles.imufilenamewrist);
    handles.imudatawrist = struct('time',time,'p',p,'q',q,'indsFF',indsFF);
    handles.imudataempty = handles.imudatawrist;  handles.imudataempty.p = NaN*handles.imudataempty.p; handles.imudataempty.q = repmat([1 0 0 0],size(handles.imudataempty.q,1),1); handles.imudataempty.indsFF = []; handles.imudataempty.source = 'Wrist';
    catch
        'no wrist imu'
    end
end

if isequal(handles.imudataleft, 0);
    handles.imudataleft = handles.imudataempty;
end
if isequal(handles.imudataright, 0);
    handles.imudataright = handles.imudataempty;
end
if isequal(handles.imudatatrunk, 0);
    handles.imudatatrunk = handles.imudataempty;
end
if isequal(handles.imudatawrist, 0);
    handles.imudatawrist = handles.imudataempty;
end


VideoSync=[get(handles.videostartseg,'Value'),get(handles.videoendseg,'Value')];
IMUSyncLeft = [get(handles.imustartseg_left,'Value'),get(handles.imuendseg_left,'Value')];  indsIMULeft = IMUSyncLeft(1):IMUSyncLeft(end) ; %if indsIMULeft == 0; indsIMULeft = []; end
IMUSyncRight = [get(handles.imustartseg_right,'Value'),get(handles.imuendseg_right,'Value')];  indsIMURight = IMUSyncRight(1):IMUSyncRight(end) ; %if indsIMURight == 0; indsIMURight = []; end
% IMUSyncTrunk = [get(handles.imustartseg_trunk,'Value'),get(handles.imuendseg_trunk,'Value')];  indsIMUTrunk = IMUSyncTrunk(1):IMUSyncTrunk(end) ;
indsIMUTrunk = get(handles.imustartseg_trunk,'Value'):get(handles.imuendseg_trunk,'Value') ; if length(indsIMUTrunk)==1; indsIMUTrunk = eval(['indsIMU'  handles.imudataempty.source ]); end
% IMUSyncWrist = [get(handles.imustartseg_wrist,'Value'),get(handles.imuendseg_wrist,'Value')];  indsIMUWrist = IMUSyncWrist(1):IMUSyncWrist(end) ;
indsIMUWrist = get(handles.imustartseg_wrist,'Value'):get(handles.imuendseg_wrist,'Value') ; if length(indsIMUWrist)==1; indsIMUWrist =  eval(['indsIMU'  handles.imudataempty.source ]); end

imuframerateleft = mean(1./diff(handles.imudataleft.time));
imuframerateright = mean(1./diff(handles.imudataright.time));
imuframeratetrunk = mean(1./diff(handles.imudatatrunk.time));
imuframeratewrist = mean(1./diff(handles.imudatawrist.time));

imuframerate = nanmean([ imuframerateleft, imuframerateright, imuframeratetrunk, imuframeratewrist ]);

if get(handles.timebutton,'Value')==1
        set(handles.videostartseg,'Value',get(handles.videostartseg,'Value')*fvid);
        set(handles.videoendseg,'Value',get(handles.videoendseg,'Value')*fvid);
end

FFindleft = handles.imudataleft.indsFF - IMUSyncLeft(1) + 1 ; FFindleft(FFindleft<1) = []; FFindleft(FFindleft > diff(IMUSyncLeft([1 end]))+1) = []; FFindleft = unique(FFindleft);
FFindright = handles.imudataright.indsFF  - IMUSyncRight(1) + 1; FFindright(FFindright<1) = []; FFindright(FFindright > diff(IMUSyncRight([1 end]))+1) = []; FFindright = unique(FFindright); 

timeleft = handles.imudataleft.time(indsIMULeft,:); 
timeright = handles.imudataright.time(indsIMURight,:);
quatsleft = handles.imudataleft.q(indsIMULeft,:);
quatsright = handles.imudataright.q(indsIMURight,:);
    quatstrunk = handles.imudatatrunk.q(indsIMUTrunk,:);
    quatswrist = handles.imudatawrist.q(indsIMUWrist,:);
Pxyzleft = handles.imudataleft.p(indsIMULeft,:); 
Pxyzright = handles.imudataright.p(indsIMURight,:); 
Pxyzleft = Pxyzleft - ones(size(Pxyzleft(:,1)))*Pxyzleft(1,:);
Pxyzright = Pxyzright - ones(size(Pxyzright(:,1)))*Pxyzright(1,:);

% fourstridedistanceleft = normvec(Pxyzleft(FFindleft(5:end),1:2) - Pxyzleft(FFindleft(1:end-4),1:2) , 2) ;
% firstlongforwardwalkleft = find(fourstridedistanceleft  > prctile(fourstridedistanceleft, 80),  1); 
% fourstridedistanceright = normvec(Pxyzright(FFindright(5:end),1:2) - Pxyzright(FFindright(1:end-4),1:2) , 2) ;
% firstlongforwardwalkright = find(fourstridedistanceright  > prctile(fourstridedistanceright, 80),  1); 
% walkdirleft = Pxyzleft(FFindleft(firstlongforwardwalkleft+4),:) - Pxyzleft(FFindleft(firstlongforwardwalkleft),:) ;
% walkdirright = Pxyzright(FFindright(firstlongforwardwalkright+4),:) - Pxyzright(FFindright(firstlongforwardwalkright),:) ;
% walkangleleft = atan2(walkdirleft(2), walkdirleft(1));
% walkangleright = atan2(walkdirright(2), walkdirright(1));
  dPleft = diff(Pxyzleft(FFindleft,:),1,1) ;
  stridedistanceleft = normvec(dPleft(:,1:2) , 2) ;
  longstridesleft = find(inrange(stridedistanceleft ,[prctile(stridedistanceleft,80), prctile(stridedistanceleft,99)]));
  walkdirsleft = cell2mat(cellfun(@(dp,q) [dp 0]*quatWXYZtoRot(q), num2cell( dPleft(longstridesleft,1:2) ,2), num2cell(quatsleft(FFindleft(longstridesleft),:),2), 'UniformOutput', 0) );
%   walkanglesleft = (atan2(walkdirsleft(:,2), walkdirsleft(:,1)));
    walkdirleft = mean(walkdirsleft); walkdirleft = walkdirleft/normvec(walkdirleft); 
  walkdirsleftwithin45deg = walkdirsleft( dot(repmat(walkdirleft,size(walkdirsleft,1),1), walkdirsleft,2)./normvec(walkdirsleft,2) >= cosd(45) , :) ;
    walkdirleft = mean(walkdirsleftwithin45deg); walkdirleft = walkdirleft/normvec(walkdirleft);
  walkdirsleftwithin20deg = walkdirsleft( dot(repmat(walkdirleft,size(walkdirsleft,1),1), walkdirsleft,2)./normvec(walkdirsleft,2) >= cosd(20) , :) ;
    walkdirleft = mean(walkdirsleftwithin20deg); walkdirleft = walkdirleft/normvec(walkdirleft);
    walkdirleftWorld = quatWXYZtoRot(quatsleft(1,:))*walkdirleft' ;
  walkangleleft = atan2(walkdirleftWorld(2), walkdirleftWorld(1));
%   [t,r] = rose(walkanglesleft,ceil(length(walkanglesleft)/5)); % pick number of bins to allot 5 strides per bin if possible
  dPright = diff(Pxyzright(FFindright,:),1,1) ;
  stridedistanceright = normvec(dPright(:,1:2) , 2) ;
  longstridesright = find(inrange(stridedistanceright ,[prctile(stridedistanceright,80), prctile(stridedistanceright,99)]));
  walkdirsright = cell2mat(cellfun(@(dp,q) [dp 0]*quatWXYZtoRot(q), num2cell( dPright(longstridesright,1:2) ,2), num2cell(quatsright(FFindright(longstridesright),:),2), 'UniformOutput', 0) );
%   walkanglesright = (atan2(walkdirsright(:,2), walkdirsright(:,1)));
    walkdirright = mean(walkdirsright); walkdirright = walkdirright/normvec(walkdirright); 
  walkdirsrightwithin45deg = walkdirsright( dot(repmat(walkdirright,size(walkdirsright,1),1), walkdirsright,2)./normvec(walkdirsright,2) >= cosd(45) , :) ;
    walkdirright = mean(walkdirsrightwithin45deg); walkdirright = walkdirright/normvec(walkdirright);
  walkdirsrightwithin20deg = walkdirsright( dot(repmat(walkdirright,size(walkdirsright,1),1), walkdirsright,2)./normvec(walkdirsright,2) >= cosd(20) , :) ;
    walkdirright = mean(walkdirsrightwithin20deg); walkdirright = walkdirright/normvec(walkdirright);
    walkdirrightWorld = quatWXYZtoRot(quatsright(1,:))*walkdirright' ;
  walkangleright = atan2(walkdirrightWorld(2), walkdirrightWorld(1));
%   [t,r] = rose(walkanglesright,ceil(length(walkanglesright)/5)); % pick number of bins to allot 5 strides per bin if possible
rotToXforwardleft = [cos(walkangleleft), -sin(walkangleleft), 0;  sin(walkangleleft), cos(walkangleleft), 0; 0, 0, 1]'; % Pobserved(3x1) = rotToXforward*Pxforward(3x1)
rotToXforwardright = [cos(walkangleright), -sin(walkangleright), 0;  sin(walkangleright), cos(walkangleright), 0; 0, 0, 1]'; % Pobserved(3x1) = rotToXforward*Pxforward(3x1)
Pleftxforward = Pxyzleft*rotToXforwardleft'; 
Prightxforward = Pxyzright*rotToXforwardright'; 
 Prightxforward(:,2) = Prightxforward(:,2);
PleftxforwardFF = NaN*Pleftxforward; 
  PleftxforwardFF(FFindleft,:) = Pleftxforward(FFindleft,:) ; % To make Markers for Footfalls
PrightxforwardFF = NaN*Prightxforward; 
  PrightxforwardFF(FFindright,:) = Prightxforward(FFindright,:) ; % To make Markers for Footfalls

%   Show the rotated foot trajectories. 
% figure; plotxyz(Pxyzleft,'b'); hold on; plotxyz(Pxyzright,'g'); plotxyz(Pleftxforward,'b','LineWidth',2); plotxyz(Prightxforward,'g','LineWidth',2); axis equal; axis vis3d; 

% Eliminate outliers
minstridedistance = 0.1; % m
maxstrideheightchange = 0.5; % m
minstridetime = 0.5; % sec
maxstridetime = 2.0; % sec
rejectionslope = 0.0581; % slope of L vs T below which there appear to be a lot of artifacts in a stridedistance vs stridetime plot.
rejectionoffset = 0.001; % m step length above that line, where we set the cutoff. 

% stridedistanceleft, stridedistanceright
stridetimeleft = diff(timeleft(FFindleft));
stridetimeright = diff(timeright(FFindright));

allstridedistance = [stridedistanceleft;stridedistanceright];

mdnstridedistance = median(allstridedistance(inrange(allstridedistance ,[0.5 5.0],[1 1])));
sdstridedistance = std(allstridedistance(inrange(allstridedistance ,[0.5 5.0],[1 1])));

maxstridedistance = mdnstridedistance + 2*sdstridedistance;

% mnstridetime = mean([stridetimeleft;stridetimeright]); 
% sdstridetime = std([stridetimeleft;stridetimeright]); 


% keepleft and keepright will be logicals into FFindleft and FFindright, or
% into the stride metrics formed from 
keepStridesleft = find( inrange(stridedistanceleft,[minstridedistance,maxstridedistance],[1 1]) & inrange(stridetimeleft, [minstridetime, maxstridetime],[1 1]) & (stridedistanceleft > rejectionslope*stridetimeleft+rejectionoffset) & (abs(dPleft(:,3)) < maxstrideheightchange) ) ; 
keepStridesright = find( inrange(stridedistanceright,[minstridedistance,maxstridedistance],[1 1]) & inrange(stridetimeright, [minstridetime, maxstridetime],[1 1]) & (stridedistanceright > rejectionslope*stridetimeright+rejectionoffset) & (abs(dPright(:,3)) < maxstrideheightchange) ) ;  
keepFFleft = unique([keepStridesleft;keepStridesleft+1]);
keepFFright = unique([keepStridesright;keepStridesright+1]);


% Compute distance walked. 
% Lleftstride = [0; normvec(diff(PleftxforwardFF(FFindleft,1:2),1,1), 2)]; 
% distleft = cumsum(Lleftstride); 
distleft = cumsum( [0; stridedistanceleft]);
% Lrightstride = [0; normvec(diff(PrightxforwardFF(FFindright,1:2),1,1), 2)]; 
% distright = cumsum(Lrightstride); 
distright = cumsum( [0; stridedistanceright]);
% V1stride = L1stride ./ diff(handles.imudataleft.indsFF*PERIOD); 
% maxLstride = max([Lleftstride; Lrightstride]); 
maxLstride = max([stridedistanceleft;stridedistanceright]);
axoffscale = 1.5; 
axisoffsets = [-axoffscale*maxLstride, axoffscale*maxLstride, -axoffscale*maxLstride, axoffscale*maxLstride,  -0.4, 2.4] ; 


%% Rotate the Foot Graphic so that Up is Up
% % rotfootzup = quatWXYZtoRot(quatsleft(1,:))'; % this should be applied to rotate the foot graphic so its orientation is up *Assuming it is so in the first frame of data.*
% % rotfootzupleft = rodrigues(mean( cell2mat(cellfun(@(q) rodrigues(quatWXYZtoRot(q)')', num2cell(quatsleft(FFindleft(firstlongforwardwalkleft+[0:4]),:),2),'UniformOutput',0))*diag([1 1 1]) ));  % this should be applied to rotate the foot graphic so its orientation is up *Assuming it is so at the first several Footfalls.*
% % rotfootzupright = rodrigues(mean( cell2mat(cellfun(@(q) rodrigues(quatWXYZtoRot(q)')', num2cell(quatsright(FFindright(firstlongforwardwalkright+[0:4]),:),2),'UniformOutput',0))*diag([1 1 1]) ));
% Original working: 
% rotfootzupleft = rodrigues(mean( cell2mat(cellfun(@(q) rodrigues(quatWXYZtoRot(q))', num2cell(quatsleft(FFindleft(firstlongforwardwalkleft+[0:4]),:),2),'UniformOutput',0))*diag([1 1 1]) ));
% rotfootzupright = rodrigues(mean( cell2mat(cellfun(@(q) rodrigues(quatWXYZtoRot(q))', num2cell(quatsright(FFindright(firstlongforwardwalkright+[0:4]),:),2),'UniformOutput',0))*diag([1 1 1]) ));
% Based on All Footfalls
rotfootzupleft = rodrigues(mean( cell2mat(cellfun(@(q) (cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1])'/norm(cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1]))*acos(dot(quatWXYZtoRot(q)*[0;0;1], [0;0;1])))*quatWXYZtoRot(q), num2cell(quatsleft(FFindleft(longstridesleft),:),2),'UniformOutput',0)) ));  % this should be applied to rotate the foot graphic so its orientation is up *Assuming it is so during Long Footfalls.*
rotfootzupright = rodrigues(mean( cell2mat(cellfun(@(q) (cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1])'/norm(cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1]))*acos(dot(quatWXYZtoRot(q)*[0;0;1], [0;0;1])))*quatWXYZtoRot(q), num2cell(quatsright(FFindright(longstridesright),:),2),'UniformOutput',0)) ));  % this should be applied to rotate the foot graphic so its orientation is up *Assuming it is so during Long Footfalls.*

%% Compute all the rotations
% hf = figure; set(hf,'Position',[680 905 560 90]); ha = axes; set(ha,'Units','normalized'); set(ha,'Position',[0.1 0.3 0.8 0.4],'XLim',[0 100],'YLim',[0 1],'YTick',[]); hpatch = patch([0 0 0 0 0],[0 0 1 1 0],'g'); title('Processing...','FontWeight','Bold');
% h=figure; [hfootpatch] = footpatch3([],footSide,zeros(1,length(comps)),eye(length(comps)),0.2921); 
% for ii = 1:length(Pleftxframeinds)
% set(hpatch,'XData',(ii/length(Pleftxframeinds))*100*[0 1 1 0 0]); drawnow;
%  %2/7/2014
% %      qrot{ii} = rot*quatWXYZtoRot(quatsleft(Pleftxframeinds(ii),:))*rot'  ;%* rotg * rotx' * rodrigues([0 0 pi]) ; % *diag([-1 -1 1]) (use the extra diag() for MEMSense mounted with +X toward the rear). 
% % 2014-06-19 edit: added "rotfootzup" to fix the fact that the may have its axes tilted with respect to the foot. 
%      qrot{ii} = rot*quatWXYZtoRot(quatsleft(Pleftxframeinds(ii),:))*rotfootzup*rot'  ; 
%      [hfootpatch,xyzfootpatch{ii}] = footpatch3(hfootpatch,footSide,Pleftxforward(Pleftxframeinds(ii),comps), qrot{ii}(comps,comps)', 0.2921);
%  end
%  close(h);
%  close(hf); 


%% Now to make the steps line up correctly for Left and Right
%%% Fix the Filtering for Edge Effects!? 

% % qrotleft = rotToXforwardleft*quatWXYZtoRot(quatsleft(Pxframeinds(ind),:))*rotfootzupleft*rotToXforwardleft'  ; 
[distleft,idistleft,~] = unique(distleft);  
FFindleftunique = FFindleft(idistleft);
rodleftFF = cell2mat(cellfun(@(q) rodrigues(rotToXforwardleft*quatWXYZtoRot(q)*rotfootzupleft'*rotToXforwardleft')' , num2cell(quatsleft(FFindleftunique,:),2),'UniformOutput',0)); 
yawleftFF = unwrap(rodleftFF(:,3));
% disttimeleft = interp1(distleft, timeleft(FFindleftunique,:), [distleft(1):0.2:distleft(end), distleft(end)]','linear','extrap');
% yawleft = interp1(distleft, yawleftFF, [distleft(1):0.2:distleft(end), distleft(end)]','linear','extrap');

[distright,idistright,~] = unique(distright);
FFindrightunique = FFindright(idistright);
rodrightFF = cell2mat(cellfun(@(q) rodrigues(rotToXforwardright*quatWXYZtoRot(q)*rotfootzupright'*rotToXforwardright')' , num2cell(quatsright(FFindrightunique,:),2),'UniformOutput',0)); 
yawrightFF = unwrap(rodrightFF(:,3));
% disttimeright = interp1(distright, timeright(FFindrightunique,:), [distright(1):0.2:distright(end), distright(end)]','linear','extrap');
% yawright = interp1(distright, yawrightFF, [distright(1):0.2:distright(end), distright(end)]','linear','extrap');

yawinds = union(FFindleftunique, FFindrightunique); %round(interp1(distleft, FFindleftunique, [distleft(1):0.2:distleft(end), distleft(end)]', 'linear','extrap'));
yawleft = interp1(FFindleftunique, yawleftFF, yawinds,'linear','extrap');
yawright = interp1(FFindrightunique, yawrightFF, yawinds,'linear','extrap');

% Fix Yaw for Trunk and Arm
uprighttrunkmat = [0 0 -1; 1 0 0; 0 -1 0]; % this works in the sense that a vector aimu(3x1) in IMU frame can be converted to World frame thus: aworld = uprighttorsomat *aimu
yawindstrunk = yawinds(yawinds <= length(quatstrunk)); 
rodtrunkFF = cell2mat(cellfun(@(q) rodrigues(quatWXYZtoRot(q)*uprighttrunkmat')' , num2cell(quatstrunk(yawindstrunk,:),2),'UniformOutput',0)); % the rot(q)*uprighttorsomat' term gives the rotation that needs to be applied to make an upright torso 
yawtrunk = interp1(yawindstrunk, unwrap(rodtrunkFF(:,3)), yawinds, 'linear','extrap');

% % % Because Trunk and Wrist are both using a Magnetic North reference, they
% % % should track each other. So I'm going to try to just use the Trunk
% % % correction for the Wrist as well. 
% % thumbforwardwristmatLeft = [0 -1 0; 0 0 1; -1 0 0];
% % thumbforwardwristmatRight = [0 1 0; 0 0 -1; -1 0 0]; % this works in the sense that a vector aimu(3x1) in IMU frame can be converted to World frame this: aworld = thumbforwardarmmat *aimu
% % rodwristFF = cell2mat(cellfun(@(q) rodrigues(quatWXYZtoRot(q)*uprightwristmat')' , num2cell(quatswrist(yawinds,:),2),'UniformOutput',0)); % the rot(q)*uprighttorsomat' term gives the rotation that needs to be applied to make an upright torso 
% % yawwrist = unwrap(rodwristFF(:,3));
% 
% 
% %     disttimeleft = timeleft(FFindleftunique,:);
% %     yawleft = yawleftFF;
% %     disttimeright = timeright(FFindrightunique,:);
% %     yawright = yawrightFF;
% 
% % % combinedtime = unique([disttimeleft; disttimeright]);
% % % yawmean = mean( [interp1(disttimeleft, yawleft, combinedtime,'linear','extrap'), interp1(disttimeright, yawright, combinedtime,'linear','extrap')], 2);
% % yawmean = mean( [interp1(disttimeleft, yawleftdist, combinedtime,'linear','extrap'), interp1(disttimeright, yawrightdist, combinedtime,'linear','extrap')], 2);
% 
% yawmean = mean([yawleft,yawright],2);
% 
% % Find out how far each foot's yaw is from the Mean. 
% % epsyawleft = yawleftdist - interp1(combinedtime, yawmean, disttimeleft,'linear','extrap');  
% % epsyawright = yawrightdist - interp1(combinedtime, yawmean, disttimeright,'linear','extrap');  
% epsyawleft = yawleftFF - interp1(yawinds, yawmean, FFindleftunique,'linear','extrap');  
% epsyawright = yawrightFF - interp1(yawinds, yawmean, FFindrightunique,'linear','extrap');  
% 
% epsyawtrunk = yawtrunk - yawmean;  % this should fix the rotation away from Forward, shouldn't it? 
% 
% % Interpolate and Filter a Yaw Correction at all time:
% [B,A] = butter(1, 0.1/(imuframerate/2) );
% % fixyawleft = filtfilt(B,A,interp1(disttimeleft, -epsyawleft, timeleft, 'linear', 'extrap'));
% % fixyawright = filtfilt(B,A,interp1(disttimeright, -epsyawright, timeright, 'linear', 'extrap'));
% fixyawleft = filtfilt(B,A,interp1(FFindleftunique, -epsyawleft, (1:size(Pleftxforward,1))', 'linear', 'extrap'));
% fixyawright = filtfilt(B,A,interp1(FFindrightunique, -epsyawright, (1:size(Prightxforward,1))', 'linear', 'extrap'));
% fixyawtrunk = filtfilt(B,A,interp1(yawinds, -epsyawtrunk, (1:size(Prightxforward,1))', 'linear', 'extrap'));
% 
% fixyawwrist = fixyawtrunk;
% 
% % Correct Linear Positions
% Pinds = yawinds;
% dPleft = [0 0 0; diff(Pleftxforward,1,1)];
% dPright = [0 0 0; diff(Prightxforward,1,1)]; 
% 
% dPleft = cell2mat(cellfun(@(dP,fY) dP*rodrigues([0 0 fY])' , num2cell(dPleft,2), num2cell(fixyawleft,2), 'UniformOutput',0) );
% dPright = cell2mat(cellfun(@(dP,fY) dP*rodrigues([0 0 fY])' , num2cell(dPright,2), num2cell(fixyawright,2), 'UniformOutput',0) );
% 
% Pleft = cumsum(dPleft);
% Pright = cumsum(dPright);
% 
% % Pnomleft = interp1(distleft, Pleft(FFindleftunique,:), [distleft(1):0.2:distleft(end), distleft(end)]', 'linear', 'extrap');
% % Pnomright = interp1(distright, Pright(FFindrightunique,:), [distright(1):0.2:distright(end), distright(end)]', 'linear', 'extrap');
% % Pnommean = mean( cat(3, interp1(disttimeleft, Pnomleft, combinedtime, 'linear', 'extrap'), interp1(disttimeright, Pnomright, combinedtime, 'linear', 'extrap')), 3);
% Pnomleft = interp1(FFindleftunique, Pleft(FFindleftunique,:), Pinds,'linear','extrap');
% Pnomright = interp1(FFindrightunique, Pright(FFindrightunique,:), Pinds,'linear','extrap');
% Pnommean = mean( cat(3, Pnomleft, Pnomright), 3);
% 
% %     disttimeleft = timeleft(FFindleftunique,:);
% %     disttimeright = timeright(FFindrightunique,:);
% %     Pnomleft = Pleft(FFindleftunique,:);
% %     Pnomright = Pright(FFindrightunique,:);
% %     Pnommean = mean( cat(3, interp1(disttimeleft, Pnomleft, combinedtime, 'linear', 'extrap'), interp1(disttimeright, Pnomright, combinedtime, 'linear', 'extrap')), 3);
% 
% Pnommeanleft = interp1(Pinds, Pnommean, FFindleftunique,'linear','extrap'); 
% Pnommeanright = interp1(Pinds, Pnommean, FFindrightunique, 'linear', 'extrap'); 
% 
% Ptargetleft = cell2mat(cellfun(@(p,Y) p + [0 0.075 0]*rodrigues([0 0 Y])' , num2cell(Pnommeanleft,2), num2cell(yawleftFF-epsyawleft,2), 'UniformOutput',0)); 
% Ptargetright = cell2mat(cellfun(@(p,Y) p + [0 -0.075 0]*rodrigues([0 0 Y])' , num2cell(Pnommeanright,2), num2cell(yawrightFF-epsyawright,2), 'UniformOutput',0)); 
% % figure; plotxyz(Pleft); hold on; plotxyz(Pright,'g'); plotxyz(Pnommean,'k'); plotxyz(Ptargetleft,'b:'); plotxyz(Ptargetright,'g:'); axis equal
% 
% % epsPleft = interp1(timeleft,Pleft, disttimeleft) - Ptargetleft;
% % epsPright = interp1(timeright,Pright, disttimeright) - Ptargetright;
% epsPleft = Pleft(FFindleftunique,:) - Ptargetleft;
% epsPright = Pright(FFindrightunique,:) - Ptargetright;
% 
% % fixPleft = filtfilt(B,A,interp1(FFindleftunique, -epsPleft, timeleft, 'linear', 'extrap'));
% % fixPright = filtfilt(B,A,interp1(FFindrightunique, -epsPright, timeright, 'linear', 'extrap'));
% fixPleft = filtfilt(B,A,interp1(FFindleftunique, -epsPleft, (1:size(Pleftxforward,1))', 'linear', 'extrap'));
% fixPright = filtfilt(B,A,interp1(FFindrightunique, -epsPright, (1:size(Prightxforward,1))', 'linear', 'extrap'));
% 
% fixGain = 1
% Pleftfixed = Pleft + fixGain*fixPleft;
% Prightfixed = Pright + fixGain*fixPright;
% % figure; plotxyz(Pleftxforward,'b','LineWidth',2); hold on; plotxyz(Prightxforward, 'g', 'LineWidth',2); plotxyz(Pleft); plotxyz(Pright,'g'); plotxyz(Pnommean,'k'); plotxyz(Ptargetleft,'b:'); plotxyz(Ptargetright,'g:'); hold on; plotxyz(Pleftfixed,'c','LineWidth',2); plotxyz(Prightfixed,'m','LineWidth',2); axis equal;
% 
% % Reassign Forward direction. 
% Pleftxforward = Pleftfixed;    
% Prightxforward = Prightfixed;  
% PleftxforwardFF(FFindleft,:) = Pleftxforward(FFindleft,:) ; % To make Markers for Footfalls
% PrightxforwardFF(FFindright,:) = Prightxforward(FFindright,:) ; % To make Markers for Footfalls

%% Much like "rotfootzup", compute the inclination of the foot with respect to gravity
% footzvecLeftFF = cell2mat(cellfun(@(q) quatWXYZtoRot(q)*[0;0;1], num2cell(quatsleft(FFindleft,:),2),'UniformOutput',0)) ));  % this should be give the tip of the foot's Z vector in world space
% footzvecRightFF = cell2mat(cellfun(@(q) quatWXYZtoRot(q)*[0;0;1], num2cell(quatsright(FFindright,:),2),'UniformOutput',0)) ));  % this should be applied to rotate the foot graphic so its orientation is up *Assuming it is so during Long Footfalls.*

% % restricted to only 80th to 99th percentile stride lengths
% rodLeftFF = cell2mat(cellfun(@(q) (cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1])'/norm(cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1]))*acos(dot(quatWXYZtoRot(q)*[0;0;1], [0;0;1])))*quatWXYZtoRot(q), num2cell(quatsleft(FFindleft(longstridesleft),:),2),'UniformOutput',0)) ;  % Rotation in the foot frame (rodrigues vector) of difference between foot Z and gravity Z, reported in the Foot Frame. 
% rodRightFF = cell2mat(cellfun(@(q) (cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1])'/norm(cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1]))*acos(dot(quatWXYZtoRot(q)*[0;0;1], [0;0;1])))*quatWXYZtoRot(q), num2cell(quatsright(FFindright(longstridesright),:),2),'UniformOutput',0)) ;  % Rotation in the foot frame (rodrigues vector) of difference between foot Z and gravity Z, reported in the Foot Frame. 
% % including all footfalls
% rodLeftFF = cell2mat(cellfun(@(q) (cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1])'/norm(cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1]))*acos(dot(quatWXYZtoRot(q)*[0;0;1], [0;0;1])))*quatWXYZtoRot(q), num2cell(quatsleft(FFindleft,:),2),'UniformOutput',0)) ;  % Rotation in the foot frame (rodrigues vector) of difference between foot Z and gravity Z, reported in the Foot Frame. 
% rodRightFF = cell2mat(cellfun(@(q) (cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1])'/norm(cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1]))*acos(dot(quatWXYZtoRot(q)*[0;0;1], [0;0;1])))*quatWXYZtoRot(q), num2cell(quatsright(FFindright,:),2),'UniformOutput',0)) ;  % Rotation in the foot frame (rodrigues vector) of difference between foot Z and gravity Z, reported in the Foot Frame. 
% including all footfalls at either the Start or End of a "keepleft" or
% "keepright" stride. 
rodLeftFF = cell2mat(cellfun(@(q) (cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1])'/norm(cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1]))*acos(dot(quatWXYZtoRot(q)*[0;0;1], [0;0;1])))*quatWXYZtoRot(q), num2cell(quatsleft(FFindleft(keepFFleft),:),2),'UniformOutput',0)) ;  % Rotation in the foot frame (rodrigues vector) of difference between foot Z and gravity Z, reported in the Foot Frame. 
rodRightFF = cell2mat(cellfun(@(q) (cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1])'/norm(cross(quatWXYZtoRot(q)*[0;0;1], [0;0;1]))*acos(dot(quatWXYZtoRot(q)*[0;0;1], [0;0;1])))*quatWXYZtoRot(q), num2cell(quatsright(FFindright(keepFFright),:),2),'UniformOutput',0)) ;  % Rotation in the foot frame (rodrigues vector) of difference between foot Z and gravity Z, reported in the Foot Frame. 


%% Compute and plot lots of metrics. 
% yawleftFF 
% yawrightFF
% yawtrunk 
% Pleftxforward
% Prightxforward
% PleftxforwardFF
% PrightxforwardFF
% rodLeftFF
% rodRightFF
% Lleftstride
% Lrightstride

% Ground Unevenness: Tilt of Foot at FootFall (with respect to Gravity)
figure; plotxyz(-1*rodLeftFF(:,1:2),'.'); axis equal; title('Left Rodrigues Vector at FootFall: Rotation from Gravity to Foot Z'); 
figure; plotxyz(-1*rodRightFF(:,1:2),'.'); axis equal; title('Right Rodrigues Vector at FootFall: Rotation from Gravity to Foot Z'); 

% Parameters: 
maxbins = 40;

tic
% Foot Clearance - PDF and CDF
% Left
FFleftUseToStart = FFindleft;%(keepStridesleft);
for ii = length(FFleftUseToStart)-1 :-1:1
    inds = FFleftUseToStart(ii):FFleftUseToStart(ii+1) ; 
%     pos = Pleftxforward(inds,:) ; 
    pos = Pxyzleft(inds,:) ; 
    qua = quatsleft(inds,:); 
    inds = (1:size(pos,1))' ; 
    
    footDisplacementLeft(ii,:) = diff(pos([1 end],:)) ; 
%     footDisplacementLeftInFootFrame(ii,:) = footDisplacementLeft(ii,:)*quatWXYZtoRot(qua(1,:)) ; 
%     footDisplacementLeftInXforwZupFootFrame(ii,:) = footDisplacementLeft(ii,:)*quatWXYZtoRot(qua(1,:))*rotfootzupleft ;
      imuxvec = quatWXYZtoRot(qua(1,:))*[1;0;0]; imuxvec = imuxvec/norm(imuxvec);
      imuxhorizzup = [imuxvec, cross([0;0;1],imuxvec), [0;0;1] ] ; % cols are unit vectors for [xnative_horiz, yfoot, zfoot[
    footDisplacementLeftInXhorizZupFootFrame(ii,:) = footDisplacementLeft(ii,:)*imuxhorizzup ;
    orientationChange = rodrigues(quatWXYZtoRot(qua(end,:)) * quatWXYZtoRot(qua(1,:))') ; 
    footOrientationChangeLeft(ii,:) = orientationChange; 
    footYawDisplacementLeft(ii) = orientationChange(3); 
    horizdistmoved = normvec(pos(:,1:2) - repmat(pos(1,1:2),length(inds),1),2) ; 
    strideLengthLeft(ii) = horizdistmoved(end) ; 
    strideTimeLeft(ii) = (length(inds) - 1) / imuframerateleft ; 
    strideVelocityLeft(ii) = strideLengthLeft(ii) / strideTimeLeft(ii); 
    
    horizDP = diff(horizdistmoved);
%     clearanceInds = inds(1) - 1 + find(inrange(horizdistmoved,[0.3 0.7]*strideLengthLeft(ii),[1 1]) ) ; 
    clearanceInds = inds(1) - 1 + find(horizDP>0.9*max(horizDP))  ; 
    if ~isempty(clearanceInds)
        imuClearanceLeft(ii) = min(pos(clearanceInds,3) - interp1(inds([1 end]), pos(inds([1 end]),3), clearanceInds) ) ;
    else
        imuClearanceLeft(ii) = NaN ; 
    end
end
% goodStridesLeft = strideLengthLeft>0.1 ; 
goodStridesLeft = keepStridesleft ; 


% Right
FFrightUseToStart = FFindright;%(keepStridesright);
for ii = length(FFrightUseToStart)-1 :-1:1
    inds = FFrightUseToStart(ii):FFrightUseToStart(ii+1) ; 
%     pos = Prightxforward(inds,:) ; 
    pos = Pxyzright(inds,:) ; 
    qua = quatsright(inds,:);
    inds = (1:size(pos,1))' ; 
    
    footDisplacementRight(ii,:) = diff(pos([1 end],:)) ; 
%     footDisplacementRightInFootFrame(ii,:) = footDisplacementRight(ii,:)*quatWXYZtoRot(qua(1,:)) ; 
%     footDisplacementRightInXforwZupFootFrame(ii,:) = footDisplacementRight(ii,:)*quatWXYZtoRot(qua(1,:))*rotfootzupright ;
      imuxvec = quatWXYZtoRot(qua(1,:))*[1;0;0]; imuxvec = imuxvec/norm(imuxvec);
      imuxhorizzup = [imuxvec, cross([0;0;1],imuxvec), [0;0;1] ] ; % cols are unit vectors for [xnative_horiz, yfoot, zfoot[
    footDisplacementRightInXhorizZupFootFrame(ii,:) = footDisplacementRight(ii,:)*imuxhorizzup ;
    orientationChange = rodrigues(quatWXYZtoRot(qua(end,:)) * quatWXYZtoRot(qua(1,:))') ; 
    footOrientationChangeRight(ii,:) = orientationChange; 
    footYawDisplacementRight(ii) = orientationChange(3); 
    horizdistmoved = normvec(pos(:,1:2) - repmat(pos(1,1:2),length(inds),1),2) ; 
    strideLengthRight(ii) = horizdistmoved(end) ; 
    strideTimeRight(ii) = (length(inds) - 1) / imuframerateright ;
    strideVelocityRight(ii) = strideLengthRight(ii) / strideTimeRight(ii);
    
    horizDP = diff(horizdistmoved);
%     clearanceInds = inds(1) - 1 + find(inrange(horizdistmoved,[0.3 0.7]*strideLengthRight(ii),[1 1]) ) ; 
    clearanceInds = inds(1) - 1 + find(horizDP>0.9*max(horizDP))  ; 
    if ~isempty(clearanceInds)
        imuClearanceRight(ii) = min(pos(clearanceInds,3) - interp1(inds([1 end]), pos(inds([1 end]),3), clearanceInds) ) ;
    else
        imuClearanceRight(ii) = NaN ; 
    end
end
% goodStridesRight = strideLengthRight>0.1 ; 
goodStridesRight = keepStridesright ; 
toc

zobj = zoom(gcf);
%% Clearance PDF and CDF
% Left
imuClearanceLeftLongStrides = imuClearanceLeft(goodStridesLeft);
edges = linspace(min(imuClearanceLeftLongStrides), max(imuClearanceLeftLongStrides), min(maxbins, max(11, floor(length(imuClearanceLeftLongStrides)/5))) );
n = histc(imuClearanceLeftLongStrides, edges) ; 
sortedClearance = sort( imuClearanceLeftLongStrides ) ; cumprobClearance = (1:length(sortedClearance))' / length(sortedClearance) ; 
figure; h1 = axes; bar(edges, n, 'histc'); title('Left Foot IMU Clearance (above interpolated rest position)'); xlabel('Clearance (m)'); ylabel('Count'); xl = xlim; 
h2 = axes; plot(h2, sortedClearance, cumprobClearance,'r','LineWidth',2); xlim(xl); ylabel('Cumulative Probability','Color',[1 0 0]);  
set(h1,'Box','off','YAxisLocation','Left','XAxisLocation','Bottom'); set(h2,'Color','none','Box','off','YAxisLocation','Right','XAxisLocation','Top','XTickLabel',{}); % linkaxes([h1 h2],'x');
zobj(end+1) = zoom(gcf); set(zobj,'ActionPreCallback',zoomalltogether('prezoom'), 'ActionPostCallback',zoomalltogether('postzoom'));

% Right 
imuClearanceRightLongStrides = imuClearanceRight(goodStridesRight);
edges = linspace(min(imuClearanceRightLongStrides), max(imuClearanceRightLongStrides), min(maxbins, max(11, floor(length(imuClearanceRightLongStrides)/5))) );
n = histc(imuClearanceRightLongStrides, edges) ; 
sortedClearance = sort( imuClearanceRightLongStrides ) ; cumprobClearance = (1:length(sortedClearance))' / length(sortedClearance) ; 
figure; h1 = axes; bar(edges, n, 'histc'); title('Right Foot IMU Clearance (above interpolated rest position)'); xlabel('Clearance (m)'); ylabel('Count'); xl = xlim; 
h2 = axes; plot(h2, sortedClearance, cumprobClearance,'r','LineWidth',2); xlim(xl); ylabel('Cumulative Probability','Color',[1 0 0]); 
set(h1,'Box','off','YAxisLocation','Left','XAxisLocation','Bottom'); set(h2,'Color','none','Box','off','YAxisLocation','Right','XAxisLocation','Top','XTickLabel',{}); %linkaxes([h1 h2],'x');
zobj(end+1) = zoom(gcf); set(zobj,'ActionPreCallback',zoomalltogether('prezoom'), 'ActionPostCallback',zoomalltogether('postzoom'));

%% Stride Length PDF and CDF
% Left
strideLengthLeftLongStrides = strideLengthLeft(goodStridesLeft); 
edges = linspace(0*min(strideLengthLeftLongStrides), max(strideLengthLeftLongStrides), min(maxbins, max(11, floor(length(strideLengthLeftLongStrides)/5))) ); % here use a minimum stride length of 0, because the values are always positive. 
n = histc(strideLengthLeftLongStrides, edges) ; 
sortedLength = sort( strideLengthLeftLongStrides ) ; cumprobLength = (1:length(sortedLength))' / length(sortedLength) ; 
figure; h1 = axes; bar(edges, n, 'histc'); title('Left Foot Stride Distance'); xlabel('Distance Moved (m)'); ylabel('Count'); xl = xlim; 
h2 = axes; plot(h2, sortedLength, cumprobLength,'r','LineWidth',2); xlim(xl); ylabel('Cumulative Probability','Color',[1 0 0]); 
set(h1,'Box','off','YAxisLocation','Left','XAxisLocation','Bottom'); set(h2,'Color','none','Box','off','YAxisLocation','Right','XAxisLocation','Top','XTickLabel',{}); % linkaxes([h1 h2],'x');
zobj(end+1) = zoom(gcf); set(zobj,'ActionPreCallback',zoomalltogether('prezoom'), 'ActionPostCallback',zoomalltogether('postzoom'));

% Right
strideLengthRightLongStrides = strideLengthRight(goodStridesRight); 
edges = linspace(0*min(strideLengthRightLongStrides), max(strideLengthRightLongStrides), min(maxbins, max(11, floor(length(strideLengthRightLongStrides)/5))) ); % here use a minimum stride length of 0, because the values are always positive. 
n = histc(strideLengthRightLongStrides, edges) ; 
sortedLength = sort( strideLengthRightLongStrides ) ; cumprobLength = (1:length(sortedLength))' / length(sortedLength) ; 
figure; h1 = axes; bar(edges, n, 'histc'); title('Right Foot Stride Distance'); xlabel('Distance Moved (m)'); ylabel('Count'); xl = xlim; 
h2 = axes; plot(h2, sortedLength, cumprobLength,'r','LineWidth',2); xlim(xl); ylabel('Cumulative Probability','Color',[1 0 0]); 
set(h1,'Box','off','YAxisLocation','Left','XAxisLocation','Bottom'); set(h2,'Color','none','Box','off','YAxisLocation','Right','XAxisLocation','Top','XTickLabel',{}); % linkaxes([h1 h2],'x');
zobj(end+1) = zoom(gcf); set(zobj,'ActionPreCallback',zoomalltogether('prezoom'), 'ActionPostCallback',zoomalltogether('postzoom'));


%% Whole-DataSet Histogram: Stride Length, Stride Velocity
strideVelocityLeftLongStrides = strideVelocityLeft(goodStridesLeft); 
strideVelocityRightLongStrides = strideVelocityRight(goodStridesRight); 

% Left
edgesV = linspace(0*min(strideVelocityLeftLongStrides), max(strideVelocityLeftLongStrides), min(maxbins, max(11, floor(length(strideVelocityLeftLongStrides)/5))) ); % here use a minimum stride length of 0, because the values are always positive. 
edgesL = linspace(0*min(strideLengthLeftLongStrides), max(strideLengthLeftLongStrides), min(maxbins, max(11, floor(length(strideLengthLeftLongStrides)/5))) ); % here use a minimum stride length of 0, because the values are always positive. 
figure; h0 = axes; hist3([strideVelocityLeftLongStrides', strideLengthLeftLongStrides'], 'Edges',{edgesV, edgesL}); hbars = findobj(gcf,'Tag', 'hist3'); view([0 0 1]); xlabel('Stride Velocity (m/s)'); ylabel('Stride Length (m)'); title('Stride Length vs. Stride Velocity Histogram - Left'); 
set(hbars,'CData',get(hbars,'ZData')); 
  zdat = get(hbars,'ZData');
  zdat(zdat==0)=NaN;
set(hbars,'ZData',zdat) ; colorbar; caxis([0 nanmax(zdat(:))]) ; 
vl = xlim; 
ll = ylim; 
set(gca,'Position', [0.32    0.32    0.46    0.60]); 

nL = histc(strideLengthLeftLongStrides, edgesL) ; 
nV = histc(strideVelocityLeftLongStrides, edgesV) ; 
sortedLength = sort( strideLengthLeftLongStrides ) ; cumprobLength = (1:length(sortedLength))' / length(sortedLength) ; 
sortedVelocity = sort( strideVelocityLeftLongStrides ) ; cumprobVelocity = (1:length(sortedLength))' / length(sortedLength) ; 
h1 = axes; barh(edgesL, nL, 'histc'); xlabel('Count'); ylim(ll); set(gca,'Position', [0.13    0.32    0.08     0.60]); 
h2 = axes; set(h2,'Position',get(h1,'Position')); plot(h2, cumprobLength, sortedLength, 'r','LineWidth',2); xlabel('Cum. Prob.','Color',[1 0 0]); set(h2,'XAxisLocation','top'); ylim(ll); xlim([0 1]); 
set(h1,'Box','off','YAxisLocation','Left','XAxisLocation','Bottom'); set(h2,'Color','none','Box','off','YAxisLocation','Right','XAxisLocation','Top','XTickLabel',{}); linkaxes([h0 h1 h2],'y');

h3 = axes; bar(edgesV, nV, 'histc'); ylabel('Count'); xlim(vl); set(gca,'Position', [0.32   0.13    0.46     0.08]);  
h4 = axes; set(h4,'Position',get(h3,'Position')); plot(h4, sortedVelocity, cumprobVelocity, 'r','LineWidth',2); ylabel('Cum. Prob.','Color',[1 0 0]); xlim(vl); ylim([0 1]); 
set(h3,'Box','off','YAxisLocation','Left','XAxisLocation','Bottom'); set(h4,'Color','none','Box','off','YAxisLocation','Right','XAxisLocation','Top','XTickLabel',{}); linkaxes([h0 h3 h4],'x');

% % Contours 
% [n,c] = hist3([strideVelocityLeftLongStrides', strideLengthLeftLongStrides'], 'Edges',{edgesV, edgesL});
% figure; contour3(c{1},c{2},n); xlabel('Stride Velocity (m/s)'); ylabel('Stride Length (m/s)'); title('Stride Length vs. Stride Velocity Histogram - Left'); 

%% Right
edgesV = linspace(0*min(strideVelocityRightLongStrides), max(strideVelocityRightLongStrides), min(maxbins, max(11, floor(length(strideVelocityRightLongStrides)/5))) ); % here use a minimum stride length of 0, because the values are always positive. 
edgesL = linspace(0*min(strideLengthRightLongStrides), max(strideLengthRightLongStrides), min(maxbins, max(11, floor(length(strideLengthRightLongStrides)/5))) ); % here use a minimum stride length of 0, because the values are always positive. 
figure; h0 = axes; hist3([strideVelocityRightLongStrides', strideLengthRightLongStrides'], 'Edges',{edgesV, edgesL}); hbars = findobj(gcf,'Tag', 'hist3'); view([0 0 1]); xlabel('Stride Velocity (m/s)'); ylabel('Stride Length (m)'); title('Stride Length vs. Stride Velocity Histogram - Right'); 
set(hbars,'CData',get(hbars,'ZData')); 
  zdat = get(hbars,'ZData');
  zdat(zdat==0)=NaN;
set(hbars,'ZData',zdat) ; colorbar; caxis([0 nanmax(zdat(:))]) ; 
vl = xlim; 
ll = ylim; 
set(gca,'Position', [0.32    0.32    0.46    0.60]); 

nL = histc(strideLengthRightLongStrides, edgesL) ; 
nV = histc(strideVelocityRightLongStrides, edgesV) ; 
sortedLength = sort( strideLengthRightLongStrides ) ; cumprobLength = (1:length(sortedLength))' / length(sortedLength) ; 
sortedVelocity = sort( strideVelocityRightLongStrides ) ; cumprobVelocity = (1:length(sortedLength))' / length(sortedLength) ; 
h1 = axes; barh(edgesL, nL, 'histc'); xlabel('Count'); ylim(ll); set(gca,'Position', [0.13    0.32    0.08     0.60]); 
h2 = axes; set(h2,'Position',get(h1,'Position')); plot(h2, cumprobLength, sortedLength, 'r','LineWidth',2); set(h2,'Color','none'); xlabel('Cum. Prob.','Color',[1 0 0]); set(h2,'XAxisLocation','top'); ylim(ll); xlim([0 1]); 
set(h1,'Box','off','YAxisLocation','Left','XAxisLocation','Bottom'); set(h2,'Color','none','Box','off','YAxisLocation','Right','XAxisLocation','Top','XTickLabel',{}); linkaxes([h0 h1 h2],'y');

h3 = axes; bar(edgesV, nV, 'histc'); ylabel('Count'); xlim(vl); set(gca,'Position', [0.32   0.13    0.46     0.08]);  
h4 = axes; set(h4,'Position',get(h3,'Position')); plot(h4, sortedVelocity, cumprobVelocity, 'r','LineWidth',2); set(h4,'Color','none'); ylabel('Cum. Prob.','Color',[1 0 0]); set(h4,'YAxisLocation','right'); xlim(vl); ylim([0 1]); 
set(h3,'Box','off','YAxisLocation','Left','XAxisLocation','Bottom'); set(h4,'Color','none','Box','off','YAxisLocation','Right','XAxisLocation','Top','XTickLabel',{}); linkaxes([h0 h3 h4],'x');

% % Contours 
% [n,c] = hist3([strideVelocityLeftLongStrides', strideLengthLeftLongStrides'], 'Edges',{edgesV, edgesL}); xlabel('Stride Velocity (m/s)'); ylabel('Stride Length (m/s)'); title('Stride Length vs. Stride Velocity Histogram - Right'); 
% figure; contour3(c{1},c{2},n); xlabel('Stride Velocity (m/s)'); ylabel('Stride Length (m/s)'); title('Stride Length vs. Stride Velocity Histogram - Right'); 





%% Foot Displacement in (prev. footfall) Foot Frame, for each Stride
% footDisplacementLeftInXhorizZupFootFrame
% footDisplacementRightInXhorizZupFootFrame
dispLeft = footDisplacementLeftInXhorizZupFootFrame(goodStridesLeft,:); 
dispRight = footDisplacementRightInXhorizZupFootFrame(goodStridesRight,:); 
figure; axes; %plotxyz(dispLeft,'.','MarkerSize',20); 
    hold on; scatterxyz(dispLeft, 90, cumsum(normvec(dispLeft,2)),'.');
    plot(xlim,0*xlim,'k'); plot(0*ylim, ylim, 'k');
    axis equal; axis vis3d; title('Left Foot Displacement in FootFall Reference Frame'); xlabel('Foot Frame X (m)'); ylabel('Foot Frame Y (m)'); zlabel('Foot Frame Z Up(m)'); 
figure; axes; %plotxyz(dispRight,'.','MarkerSize',20); 
    hold on; scatterxyz(dispRight, 90, cumsum(normvec(dispRight,2)),'.');
    plot(xlim,0*xlim,'k'); plot(0*ylim, ylim, 'k');
    axis equal; axis vis3d; title('Right Foot Displacement in FootFall Reference Frame'); xlabel('Foot Frame X (m)'); ylabel('Foot Frame Y (m)'); zlabel('Foot Frame Z Up(m)'); 


%% Yaw Orientation Change of the Foot during each Stride
% footOrientationChangeLeft
% footOrientationChangeRight
% max(11, floor(length(strideLengthRightLongStrides)/5))-1
deltaYawLeft = footYawDisplacementLeft(goodStridesLeft); 
deltaYawRight = footYawDisplacementRight(goodStridesRight); 
figure; [t,r] = rose(deltaYawLeft, min(maxbins, max(11, floor(length(deltaYawLeft)/5))-1) ); polar(t,r); title('Left Foot Yaw Change'); 
    h1 = gca; set(h1,'Position',[0.2    0.300    0.65    0.60]);
    h2 = axes; set(h2,'Position',[0.2    0.05    0.65    0.20]); hist(deltaYawLeft*180/pi, min(maxbins, max(11, floor(length(footYawDisplacementLeft)/5))-1) ); xlim([-180 180]);
    h3 = axes; set(h3, 'Position',[0.75 0.55 0.25 0.25]); r(r>0) = log10(r(r>0)); polar(t,r); title('log10(Count)')
figure; [t,r] = rose(deltaYawRight, min(maxbins, max(11, floor(length(deltaYawRight)/5))-1) ); polar(t,r); title('Right Foot Yaw Change'); 
    h1 = gca; set(h1,'Position',[0.2    0.300    0.65    0.60]);
    h2 = axes; set(h2,'Position',[0.2    0.05    0.65    0.20]); hist(deltaYawRight*180/pi, min(maxbins, max(11, floor(length(footYawDisplacementRight)/5))-1) ); xlim([-180 180]);
    h3 = axes; set(h3, 'Position',[0.75 0.55 0.25 0.25]); r(r>0) = log10(r(r>0)); polar(t,r); title('log10(Count)')

end
% End of main go_Callback



function videostartseg_Callback(hObject, eventdata, handles)
% hObject    handle to videostartseg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of videostartseg as text
%        str2double(get(hObject,'String')) returns contents of videostartseg as a double
set(handles.videostartseg,'Value',str2num(get(hObject,'String')));
guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function videostartseg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to videostartseg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function videoendseg_Callback(hObject, eventdata, handles)
% hObject    handle to videoendseg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of videoendseg as text
%        str2double(get(hObject,'String')) returns contents of videoendseg as a double
set(handles.videoendseg,'Value',str2num(get(hObject,'String')));
guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function videoendseg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to videoendseg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end



function imustartseg_left_Callback(hObject, eventdata, handles)
% hObject    handle to imustartseg_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of imustartseg_left as text
%        str2double(get(hObject,'String')) returns contents of imustartseg_left as a double
set(handles.imustartseg_left,'Value',str2num(get(hObject,'String')));
guidata(hObject, handles);
end


% --- Executes during object creation, after setting all properties.
function imustartseg_left_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imustartseg_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function imuendseg_left_Callback(hObject, eventdata, handles)
% hObject    handle to imuendseg_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of imuendseg_left as text
%        str2double(get(hObject,'String')) returns contents of imuendseg_left as a double
set(handles.imuendseg_left,'Value',str2num(get(hObject,'String')));
guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function imuendseg_left_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imuendseg_left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function imustartseg_right_Callback(hObject, eventdata, handles)
% hObject    handle to imustartseg_right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of imustartseg_right as text
%        str2double(get(hObject,'String')) returns contents of imustartseg_right as a double
set(handles.imustartseg_right,'Value',str2num(get(hObject,'String')));
guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function imustartseg_right_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imustartseg_right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function imuendseg_right_Callback(hObject, eventdata, handles)
% hObject    handle to imuendseg_right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of imuendseg_right as text
%        str2double(get(hObject,'String')) returns contents of imuendseg_right as a double
set(handles.imuendseg_right,'Value',str2num(get(hObject,'String')));
guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function imuendseg_right_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imuendseg_right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end









function sectionendseg_Callback(hObject, eventdata, handles)
% hObject    handle to sectionendseg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sectionendseg as text
%        str2double(get(hObject,'String')) returns contents of sectionendseg as a double
handles.sectionendseg=str2double(get(hObject,'String'));
guidata(hObject, handles);
end


% --- Executes during object creation, after setting all properties.
function sectionendseg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sectionendseg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function title_Callback(hObject, eventdata, handles)
% hObject    handle to title (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of title as text
%        str2double(get(hObject,'String')) returns contents of title as a double
% % % handles.title=get(hObject,'String');
% % % guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function title_CreateFcn(hObject, eventdata, handles)
% hObject    handle to title (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


% --- Executes on selection change in footside.
function footside_Callback(hObject, eventdata, handles)
% hObject    handle to footside (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns footside contents as cell array
%        contents{get(hObject,'Value')} returns selected item from footside
menuval = get(hObject, 'Value');
menustrings = cellstr( get(hObject, 'String') );
% Hints: contents = cellstr(get(hObject,'String')) returns section contents as cell array
%        contents{get(hObject,'Value')} returns selected item from section
% Determine the selected data set.
str = get(hObject, 'String');
val=get(hObject, 'Value');
switch str{val};
    case 'Choose'
        disp('You must select a foot to process');
    case 'Left' 
        handles.whichside='L';
    case 'Right' 
        handles.whichside='R';
end
% Save the handles structure.
guidata(hObject,handles)
end

% --- Executes during object creation, after setting all properties.
function footside_CreateFcn(hObject, eventdata, handles)
% hObject    handle to footside (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


menu_options={'Choose','Left','Right'};
set(hObject,'String',menu_options);
guidata(hObject, handles);
end

% --- Executes on selection change in hsmenu.
function hsmenu_Callback(hObject, eventdata, handles)
% hObject    handle to hsmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns hsmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from hsmenu
% Walk 2p0
HSWALK2p0 = [3487, 10105]; 
% Run 3p0
HSRUN3p0 = [15406, 21463]; 
% Run 4p0
HSRUN4p0 = [23373, 28725]; 

str = get(hObject, 'String');
val=get(hObject, 'Value');
switch str{val};
    case 'Selection'
        disp('You must select to process');
    case 'HSWALK2p0' 
        handles.HSSECTION=HSWALK2p0;
        handles.speed=2.0;
    case 'HSRUN3p0' 
        handles.HSSECTION=HSRUN3p0;
        handles.speed=3.0;
  case 'HSRUN4p0'
    handles.HSSECTION=HSRUN4p0;
    handles.speed=4.0;
end
% Save the handles structure.
guidata(hObject,handles)
end

% --- Executes on button press in timebutton.
function timebutton_Callback(hObject, eventdata, handles)
% hObject    handle to timebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(hObject,'Value') == get(hObject,'Max'))
   % Checkbox is checked-take appropriate action
   set(handles.timebutton,'Value',1);
guidata(hObject,handles);

else
   % Checkbox is not checked-take appropriate action
end

% Hint: get(hObject,'Value') returns toggle state of timebutton
end

% --- Executes on button press in framebutton.
function framebutton_Callback(hObject, eventdata, handles)
% hObject    handle to framebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(hObject,'Value') == get(hObject,'Max'))
   % Checkbox is checked-take appropriate action
   set(handles.timebutton,'Value',0);
guidata(hObject,handles);

else
   % Checkbox is not checked-take appropriate action
end
% Hint: get(hObject,'Value') returns toggle state of framebutton
end

% --- Executes on button press in instructions.
function instructions_Callback(hObject, eventdata, handles)
% hObject    handle to instructions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Instructions;
end


% --- Executes on button press in getTimeLimits.
function getTimeLimits_Callback(hObject, eventdata, handles)
% hObject    handle to getTimeLimits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

end


% --- Executes on button press in getFrameLimits.
function getIndexLimits_Callback(hObject, eventdata, handles)
% hObject    handle to getFrameLimits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    try  % try to get the number of video frames. 
        nvid = handles.videoObj.NumberOfFrames;
        fvid = handles.videoObj.FrameRate;
        set(handles.videostartseg,'String',sprintf('%d',1));
        set(handles.videostartseg,'Value',1);
        set(handles.videoendseg,'String',sprintf('%d',nvid));
        set(handles.videoendseg,'Value',nvid);
    catch % Will fail if there's no video or something wrong with the video.
        nvid = 0;
        fvid = 0;
    end

%     if ~isfield(handles,{'imudataleft'})
        try
            [time,p,q,indsFF] = ProcessIMUWalkCSVData(handles.imufilenameleft);
            handles.imudataleft = struct('time',time,'p',p,'q',q,'indsFF',indsFF);
            handles.imudataempty = handles.imudataleft; handles.imudataempty.p = NaN*handles.imudataempty.p; handles.imudataempty.q = repmat([1 0 0 0],size(handles.imudataempty.q,1),1); handles.imudataempty.indsFF = []; handles.imudataempty.source = 'Left';
            nimu = size(p,1);

            set(handles.imustartseg_left,'String',sprintf('%d',1));
            set(handles.imustartseg_left,'Value',1);
            set(handles.imuendseg_left,'String',sprintf('%d',nimu));
            set(handles.imuendseg_left,'Value',nimu);

        catch
            'no left foot imu'
        end
%     end
%     try % try to get the number of Left IMU frames. 
%         if ~all(isfield(handles,{'imudataleft'}))
%             [time,p,q,indsFF] = ProcessIMUWalkCSVData(handles.imufilenameleft);
%             
%             nimu = size(p,1);
%             
%             set(handles.imustartseg_left,'String',sprintf('%d',1));
%             set(handles.imustartseg_left,'Value',1);
%             set(handles.imuendseg_left,'String',sprintf('%d',nimu));
%             set(handles.imuendseg_left,'Value',nimu);
%         else
%         end
%         
%     catch % Will fail if there's no IMU file or something wrong with the IMU file.
%     end
    
%     if ~isfield(handles,{'imudataright'})
        try
            [time,p,q,indsFF] = ProcessIMUWalkCSVData(handles.imufilenameright);
            handles.imudataright = struct('time',time,'p',p,'q',q,'indsFF',indsFF);
            handles.imudataempty = handles.imudataright;  handles.imudataempty.p = NaN*handles.imudataempty.p; handles.imudataempty.q = repmat([1 0 0 0],size(handles.imudataempty.q,1),1); handles.imudataempty.indsFF = []; handles.imudataempty.source = 'Right';
            nimu = size(p,1);

            set(handles.imustartseg_right,'String',sprintf('%d',1));
            set(handles.imustartseg_right,'Value',1);
            set(handles.imuendseg_right,'String',sprintf('%d',nimu));
            set(handles.imuendseg_right,'Value',nimu);
        catch
            'no right foot imu'
        end
%     end
% try % try to get the number of Right IMU frames.
%         if ~all(isfield(handles,{'imudataright'}))
%             [time,p,q,indsFF] = ProcessIMUWalkCSVData(handles.imufilenameright);
%             
%             nimu = size(p,1);
%             
%             set(handles.imustartseg_right,'String',sprintf('%d',1));
%             set(handles.imustartseg_right,'Value',1);
%             set(handles.imuendseg_right,'String',sprintf('%d',nimu));
%             set(handles.imuendseg_right,'Value',nimu);
%         else
%         end
%         
%     catch % Will fail if there's no IMU file or something wrong with the IMU file.
%     end
    
%     if ~isfield(handles,{'imudatatrunk'})
        try
            [time,p,q,indsFF] = ProcessIMUWalkCSVData(handles.imufilenametrunk);
            handles.imudatatrunk = struct('time',time,'p',p,'q',q,'indsFF',indsFF);
            handles.imudataempty = handles.imudatatrunk;  handles.imudataempty.p = NaN*handles.imudataempty.p; handles.imudataempty.q = repmat([1 0 0 0],size(handles.imudataempty.q,1),1); handles.imudataempty.indsFF = []; handles.imudataempty.source = 'Trunk';
            nimu = size(p,1);

            set(handles.imustartseg_trunk,'String',sprintf('%d',1));
            set(handles.imustartseg_trunk,'Value',1);
            set(handles.imuendseg_trunk,'String',sprintf('%d',nimu));
            set(handles.imuendseg_trunk,'Value',nimu);
        catch
            'no trunk imu'
        end
%     end
% try % try to get the number of Trunk IMU frames.
%         if ~all(isfield(handles,{'imudatatrunk'}))
%             [time,p,q,indsFF] = ProcessIMUWalkCSVData(handles.imufilenametrunk);
%             handles.imudatatrunk = struct('time',time,'p',p,'q',q,'indsFF',indsFF);
%             handles.imudataempty = recursivenanstruct(handles.imudatatrunk);
%             
%             nimu = size(p,1);
%             
%             set(handles.imustartseg_trunk,'String',sprintf('%d',1));
%             set(handles.imustartseg_trunk,'Value',1);
%             set(handles.imuendseg_trunk,'String',sprintf('%d',nimu));
%             set(handles.imuendseg_trunk,'Value',nimu);
%         else
%         end
%         
%     catch % Will fail if there's no IMU file or something wrong with the IMU file.
%     end

%     if ~isfield(handles,{'imudatawrist'})
        try
            [time,p,q,indsFF] = ProcessIMUWalkCSVData(handles.imufilenamewrist);
            handles.imudatawrist = struct('time',time,'p',p,'q',q,'indsFF',indsFF);
            handles.imudataempty = handles.imudatatrunk;  handles.imudataempty.p = NaN*handles.imudataempty.p; handles.imudataempty.q = repmat([1 0 0 0],size(handles.imudataempty.q,1),1); handles.imudataempty.indsFF = []; handles.imudataempty.source = 'Wrist';
            nimu = size(p,1);

            set(handles.imustartseg_wrist,'String',sprintf('%d',1));
            set(handles.imustartseg_wrist,'Value',1);
            set(handles.imuendseg_wrist,'String',sprintf('%d',nimu));
            set(handles.imuendseg_wrist,'Value',nimu);
        catch
            'no wrist imu'
        end
%     end
% try % try to get the number of Wrist IMU frames.
%         if ~all(isfield(handles,{'imudatawrist'}))
%             [time,p,q,indsFF] = ProcessIMUWalkCSVData(handles.imufilenamewrist);
%             handles.imudatawrist = struct('time',time,'p',p,'q',q,'indsFF',indsFF);
%             handles.imudataempty = recursivenanstruct(handles.imudatatrunk);
%             
%             nimu = size(p,1);
%             
%             set(handles.imustartseg_wrist,'String',sprintf('%d',1));
%             set(handles.imustartseg_wrist,'Value',1);
%             set(handles.imuendseg_wrist,'String',sprintf('%d',nimu));
%             set(handles.imuendseg_wrist,'Value',nimu);
%         else
%         end
%         
%     catch % Will fail if there's no IMU file or something wrong with the IMU file.
%     end
    
    guidata(hObject,handles);
end


% --- Executes when selected object is changed in syncBasisPanel.
function syncBasisPanel_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in syncBasisPanel 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

end




function imustartseg_trunk_Callback(hObject, eventdata, handles)
% hObject    handle to imustartseg_trunk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of imustartseg_trunk as text
%        str2double(get(hObject,'String')) returns contents of imustartseg_trunk as a double
set(handles.imustartseg_trunk,'Value',str2num(get(hObject,'String')));
guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function imustartseg_trunk_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imustartseg_trunk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function imuendseg_trunk_Callback(hObject, eventdata, handles)
% hObject    handle to imuendseg_trunk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of imuendseg_trunk as text
%        str2double(get(hObject,'String')) returns contents of imuendseg_trunk as a double
set(handles.imuendseg_trunk,'Value',str2num(get(hObject,'String')));
guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function imuendseg_trunk_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imuendseg_trunk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function imustartseg_wrist_Callback(hObject, eventdata, handles)
% hObject    handle to imustartseg_wrist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of imustartseg_wrist as text
%        str2double(get(hObject,'String')) returns contents of imustartseg_wrist as a double
set(handles.imustartseg_wrist,'Value',str2num(get(hObject,'String')));
guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function imustartseg_wrist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imustartseg_wrist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function imuendseg_wrist_Callback(hObject, eventdata, handles)
% hObject    handle to imuendseg_wrist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of imuendseg_wrist as text
%        str2double(get(hObject,'String')) returns contents of imuendseg_wrist as a double
set(handles.imuendseg_wrist,'Value',str2num(get(hObject,'String')));
guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function imuendseg_wrist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imuendseg_wrist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
