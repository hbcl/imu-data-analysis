function euler_angles ... # Vector of Doubles (3)
  = quaternion_to_euler( ...
      quaternion ... # Vector of Doubles (4)
      )
  a = quaternion(1);
  b = quaternion(2);
  c = quaternion(3);
  d = quaternion(4);

  phi   = atan2(2*(a*b + c*d), ...
                a^2 - b^2 - c^2 + d^2);

  theta = asin(2*(a*c - d*b));

  psi   = atan2(2*(a*d + b*c), ...
                a^2 + b^2 - c^2 - d^2);

  euler_angles = [phi, theta, psi];
end
