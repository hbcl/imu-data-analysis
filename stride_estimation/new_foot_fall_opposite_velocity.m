function [ velocity_peak_foot_fall_frame_mask ] ... Vector of Booleans (n_frames): Foot falls from local maxima of (opposite) foot speed
  = new_foot_fall_opposite_velocity( ...
      opposite_horizontal_linear_velocity_magnitudes, ... Vector of Doubles: The speeds of the opposite foot parallel to the ground
      sampling_period, ... Double: The sampling period
      params ... FootFallOppositeVelocityDetectionParams
      )

    % (1) velocity_peak_foot_fall_frame_mask

	% Define the foot falls to be the local maxima of the (opposite) foot velocity.
	[~, peaks_idx] = findpeaks( ...
      opposite_horizontal_linear_velocity_magnitudes, ...
      'minpeakheight', params.walking_speed_minimum, ...
      'MINPEAKDISTANCE', floor((1/sampling_period) * params.foot_fall_separation_minimum) ... % TODO: can the string parameter be made lowercase?
      );
	velocity_peak_foot_fall_frame_mask = zeros(size(opposite_horizontal_linear_velocity_magnitudes))';
	velocity_peak_foot_fall_frame_mask(peaks_idx) = 1;
	velocity_peak_foot_fall_frame_mask = (velocity_peak_foot_fall_frame_mask == 1);
end % function
