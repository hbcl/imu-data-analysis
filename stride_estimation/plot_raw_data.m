function plot_raw_data(PERIOD, plt_hour, W, A, M, downsampling_factor)
    if (exist('M','var') & ~isempty(M))
        n_subplots = 2+1;
    else
        n_subplots = 2;
    end
    
    if (~exist('downsampling_factor','var'))
        downsampling_factor = 1;
    end

    if (downsampling_factor < 1)
        ME = MException('plot_raw_data:invalidDownsamplingFactor', ...
            'The provided downsampling factor (%d) must be an integer greater or equal to 1.', ...
            downsampling_factor);
        throw(ME);
    end
    
    figure,
    
    % Time
    if plt_hour
        tscale = 3600;
    else
        tscale = 1;
    end
    t = (1:size(W,1))*PERIOD/tscale;
    t_sample = downsample_columns(downsampling_factor, t);
        
    % Angular velocity -- W
    W_deg_s = W/PERIOD*180/pi;
    W_deg_s_sample = downsample_rows(downsampling_factor, W_deg_s);
    
    sh_xa(1) = subplot(n_subplots,1,1);
    plot(t_sample, W_deg_s_sample);
    grid on;
    ylabel('W [deg/s]');
    title('Body referenced inertial signals');
    
    % Linear acceleration -- A
    A_sample = downsample_rows(downsampling_factor, A);
    
    sh_xa(2) = subplot(n_subplots,1,2);
    plot(t_sample, A_sample);
    grid on;
    ylabel('A [m/s^2]');
    
    % Magnetometer readings -- M
    if (exist('M','var') & ~isempty(M))
        M_sample = downsample_rows(downsampling_factor, M);
        
        sh_xa(3) = subplot(n_subplots, 1, 3);
        plot(t_sample, M_sample);
        grid on;
        ylabel('Mag');
    end
    
    % Axes stuff
    linkaxes(sh_xa,'x');
    
    if plt_hour; xlabel_unit = 'hr'; else; xlabel_unit = 's'; end
    if (downsampling_factor > 1)
        downsampling_notice = ' (downsampled)';
    else
        downsampling_notice = '';
    end
    xlabel(strcat('time [', xlabel_unit, ']', downsampling_notice))
end

function newM = downsample_columns(downsampling_factor, M)
    newM = M(:, 1:downsampling_factor:size(M, 2));
end

function newM = downsample_rows(downsampling_factor, M)
    newM = M(1:downsampling_factor:size(M, 1), :);
end