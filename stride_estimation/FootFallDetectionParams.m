classdef FootFallDetectionParams
  % See `foot_fall.m` for a more detailed description of how these parameters
  % are used.

  properties
    % Upper threshold for the angular velocity magnitude (deg/s) for "stationary"
    % points
    foot_fall_angular_velocity_maximum; % deg/s

    % Upper threshold for the difference in magnitudes between the linear
    % acceleration and gravity (m/s^2) for "stationary" points.
    foot_fall_linear_acceleration_maximum; % m/s^2

    % Minimum duration of "non-stationary" periods between foot falls (s)
    non_foot_fall_period_minimum; % s

    % Maximum Time that can elapse During Rest Periods before looking for a new
    % foot fall (seconds; default 3*foot_fall_period_minimum) 
    % Stationary periods exceeding this time will be divided into 
    foot_fall_period_maximum; % s

    % Movement classification criteria for walking: Minimum peak foot speed
    walking_speed_minimum; % [units? integral of foot_fall_linear_acceleration_maximum]

    % The fraction of the "typical" peak horizontal foot speed a stride must
    % attain to be classified as walking
    walking_relative_peak_speed_minimum % Double, unitless
  end

  methods
    % Default values
    function obj = FootFallDetectionParams(obj)
      obj.foot_fall_angular_velocity_maximum = 30.0;
      obj.foot_fall_linear_acceleration_maximum = 1.0;
      obj.non_foot_fall_period_minimum = 0.4;
      obj.foot_fall_period_maximum = 3*obj.non_foot_fall_period_minimum;
      obj.walking_speed_minimum = 2.0;
      obj.walking_relative_peak_speed_minimum = 0.9;
    end
  end
end
