classdef FootFallOppositeVelocityDetectionParams
  % These parameters are used in `foot_fall_opposite_velocity.m`.

  properties
    % Whether to make plots of the foot fall detection results. For debugging,
    % for example.
    plot_details; % Boolean

    % The minimum time by which foot falls must be separated. Foot fall
    % candidates not sufficiently separated from the previous foot fall are
    % discarded.
    foot_fall_separation_minimum; % Double, seconds

    % Foot fall requirement: The minimum speed at which the opposite foot must
    % be moving for a foot fall candidate to be considered a foot fall. The old
    % code recommends a value of 2.0 for normal walking, and a value of 1.2 for
    % variable-speed walking.
    walking_speed_minimum; % Double, m/s

    % The maximum permissible difference in the number of steps detected by the
    % individual foot analyses.
    step_count_difference_maximum; % Integer, unitless
  end

  methods
    % Default values
    function obj = FootFallOppositeVelocityDetectionParams(obj)
      obj.plot_details = true;
      obj.foot_fall_separation_minimum = 1/2.5;
      obj.walking_speed_minimum = 2.0;
      obj.step_count_difference_maximum = 2;
    end
  end
end
