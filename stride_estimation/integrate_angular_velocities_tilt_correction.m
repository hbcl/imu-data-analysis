function quaternion_orientations ... % Matrix of Doubles (n_frames x 4)
  = integrate_angular_velocities_tilt_correction( ...
      PERIOD,                   ... % Double, sampling period
      local_angular_velocities, ... % Matrix of Doubles (n_frames x 3)
      tilt_pitch_angles,        ... % Vector of Doubles (n_frames)
      tilt_roll_angles,         ... % Vector of Doubles (n_frames)
      stationary_period_mask,   ... % Vector of Booleans (n_frames)
      tilt_correction_params    ... % TiltCorrectionKalmanFilterParams
      )
  % Integrate IMU angular velocities with tilt corrections via a Kalman filter.

  % Unpack arguments
  n_frames = size(local_angular_velocities, 1);

  % Kalman filter parameters: Variances.
  % Note: We do not track covariances but merely approximate them by scalars,
  % hence "variance" instead of "covariance".
  process_variance     = PERIOD * tilt_correction_params.gyroscope_process_variance;
  measurement_variance = PERIOD * tilt_correction_params.accelerometer_measurement_variance;

  % Kalman filter initial state
  state_mean = [1 0 0 0]; % quaternion
    % Arbitrary initial guess for the orientation of the IMU. This guess is
    % almost certainly incorrect; we expect the tilt compensation to quickly
    % correct the orientations.
  state_variance = process_variance * (5.0^2);
    % Using a larger initial variance, arbitrarily chosen, to reflect the
    % uncertainty of our initial guess.

  % Allocate outputs
  quaternion_orientations = zeros(n_frames, 4);

  % Kalman filter
  for frame_no = 1:n_frames
    % Predict
    [predicted_mean, predicted_variance] = predict_state( ...
      state_mean, state_variance, ...
      local_angular_velocities(frame_no, :), process_variance);

    % Update
    if stationary_period_mask(frame_no)
      [updated_mean, updated_variance] = update_state( ...
        predicted_mean, predicted_variance, measurement_variance, ...
        tilt_roll_angles(frame_no), tilt_pitch_angles(frame_no));
    else
      updated_mean     = predicted_mean;
      updated_variance = predicted_variance;
    end

    % Record state
    state_mean     = updated_mean;
    state_variance = updated_variance;
    quaternion_orientations(frame_no, :) = state_mean;
  end
end


function [predicted_mean,     ... % Vector of Doubles (4), quaternion
          predicted_variance] ... % Double
  = predict_state( ...
      state_mean,       ... % Vector of Doubles (4), quaternion
      state_variance,   ... % Double
      angular_velocity, ... % Vector of Doubles(3)
      process_variance  ... % Double
      )

  predicted_mean = qua_est(angular_velocity, state_mean);
  predicted_variance = state_variance + process_variance;
end


function [updated_mean,     ... % Vector of Doubles (4), quaternion
          updated_variance] ... % Double
  = update_state( ...
      predicted_mean,       ... % Vector of Doubles (4), quaternion
      predicted_variance,   ... % Double
      measurement_variance, ... % Double
      tilt_roll_angle,      ... % Double
      tilt_pitch_angle      ... % Double
      )

  kalman_gain = ...
    predicted_variance / (predicted_variance + measurement_variance);
  predicted_euler = quaternion_to_euler(predicted_mean);

  updated_mean = eul2qua([ ...
    interpolate(kalman_gain, predicted_euler(1), tilt_roll_angle), ...
    interpolate(kalman_gain, predicted_euler(2), tilt_pitch_angle), ...
    predicted_euler(3) ...
    ]);
  updated_variance = ...
      (1 - kalman_gain) * predicted_variance * (1 - kalman_gain) ...
    + kalman_gain * measurement_variance * kalman_gain;
end


function result ... % Double
  = interpolate( ...
      fraction, ... % Double, in [0, 1]
      value1,   ... % Double
      value2    ... % Double
      )
  % Linearly interpolate between two values.
  % `interpolate(0, x1, x2) = x1`
  % `interpolate(1, x1, x2) = x2`
  
  result = value1 + fraction*(value2 - value1);
end
