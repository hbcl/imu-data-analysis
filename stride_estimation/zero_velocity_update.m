function adjusted_accelerations ... % Matrix of Doubles (n_frames x 3)
  = zero_velocity_update( ...
      foot_fall_frame_mask, ... % Vector of Booleans (n_frames)
      linear_accelerations  ... % Matrix of Doubles (n_frames x 3)
      )

  % Unpack arguments
  n_frames = size(linear_accelerations, 1);

  % Allocate outputs
  adjusted_accelerations = linear_accelerations;

  % Initial iteration state
  previous_footfall_frame_no = 0;

  for frame_no = 1:n_frames
    if foot_fall_frame_mask(frame_no)
      step_samples = frame_no - previous_footfall_frame_no;
      if step_samples < 2
          continue; % Skip short duration footfalls
      end

      step_range = (previous_footfall_frame_no+1:frame_no);

      % Assume that the total change in velocity over a step is zero
      velocity_error = sum(linear_accelerations(step_range,:));

      % Correct accelerations assuming that the error is accumulated at a
      % constant rate over the duration of the step
      mean_acceleration_error = velocity_error/step_samples;
      adjusted_accelerations(step_range, :) = ...
          linear_accelerations(step_range, :) ...
        - repmat(mean_acceleration_error, step_samples, 1);

      % Update iteration state
      previous_footfall_frame_no = frame_no;
    end
  end
end
