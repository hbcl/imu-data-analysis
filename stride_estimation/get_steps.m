% Author: Lauro Ojeda, 2011-2015
function result ... % A struct holding all the results
  = get_steps( ...
      step_start, ...
      step_end, ...
      walk_info, ...
      sampling_period, ... % Double, the IMU sampling period
      step_filter_params ...
    )

  PLOT_DETAILS = 0; % TODO: for testing
  % DIRECTION_STEPS, determine the number of steps before and after current one, used to define a straight segment, this should >= 3 for best results
  DIRECTION_STEPS = 3; 
  EXTRA_FRWD_CORRECTION = 1; % This will straighten the paths perfectly

  % Unpack the walking info
  positions = walk_info.positions;
  euler_angles = walk_info.euler_angles;
  Vm = walk_info.horizontal_linear_velocity_magnitudes;

  % Make a struct to package the step information, for convenience
  steps.step_start = step_start(:);
  steps.step_end = step_end(:);
  steps.number_of_steps = size(steps.step_start,1);
  steps.max_step_nIndices = max(steps.step_end - steps.step_start) + 1;

  if(PLOT_DETAILS)
    % This is the average walk direction that is used to rotate the trajectory, is valid for short walks,
    % and when using low drift gltrl_pol_rotos
    Px =  positions(step_start, 1);
    Py =  positions(step_start, 2);
    pol = polyfit(Px, Py, 1);
    % Use the atan2 to determine the right grid quadrant
    overall_step_direction = atan2(polyval(pol, Px(end)) - polyval(pol, Px(1)), Px(end) - Px(1));
    [frwd_pol_rot, ltrl_pol_rot] = rotate_angle(Px, Py, -overall_step_direction);
    PATH_FIG = figure;
    hold on; grid on;
    plot(frwd_pol_rot, ltrl_pol_rot, 'k');
  end;

  % Create a new, empty struct, and have each of the following functions add fields to it.
  result = struct();

  [result, centered_positions_forward] = step_forward_lateral_decomposition( ...
    result, steps, positions, DIRECTION_STEPS, EXTRA_FRWD_CORRECTION, PLOT_DETAILS);

  result = step_elevation_and_pitch( ...
    result, steps, positions, euler_angles);

  result = step_speeds(result, steps, centered_positions_forward, sampling_period);

  result = step_heading(result, steps, euler_angles, PLOT_DETAILS, DIRECTION_STEPS);

  result = step_forward_lateral_decomposition_swing(result, steps, positions);

  start_end = [steps.step_start  steps.step_end];
  result.start_end = start_end';

  step_heading_change = euler_angles(steps.step_end, 3) - euler_angles(steps.step_start, 3);
  result.diff_foot_heading = step_heading_change;

  % Eliminate user defined ouliers

  OUTLIER_SECTION_SECONDS = step_filter_params.exclude_sections;
  n_outlier_sections = size(OUTLIER_SECTION_SECONDS, 1);

  OUTLIER_SECTION_SAMPLES = [];
  for ii=1:n_outlier_sections
    OUTLIER_SECTION_SAMPLES = ...
      [ OUTLIER_SECTION_SAMPLES, ...
        ( floor(OUTLIER_SECTION_SECONDS(ii, 1) / PERIOD) : ...
          floor(OUTLIER_SECTION_SECONDS(ii, 2) / PERIOD) ...
        ) ];
  end;

  out_of_bound=[];
  for(i = 1:steps.number_of_steps)
    if(sum(steps.step_start(i)==OUTLIER_SECTION_SAMPLES) | sum(steps.step_end(i)==OUTLIER_SECTION_SAMPLES))
      out_of_bound = [out_of_bound,i];
    end;
  end;

  if(~isempty(out_of_bound))
    disp('User defined outliers');
    [result, new_number_of_steps] = cut_step_section(result, out_of_bound, steps.number_of_steps);
  else
    new_number_of_steps = steps.number_of_steps;
  end;

  if(PLOT_DETAILS)
    t = (1:length(Vm))*sampling_period;
    figure;
    plot(t,Vm);hold on;
    plot(t(OUTLIER_SECTION_SAMPLES),Vm(OUTLIER_SECTION_SAMPLES),'.y');
    plot(t(steps.step_start(1)),Vm(steps.step_start(1)),'*g');
    plot(t(steps.step_end(end)),Vm(steps.step_end(end)),'*r');
    xlabel('Sample #');
    ylabel('Speed [m/sec]');
    legend('Vm','Start','End');
    grid on;
  end;

  if(step_filter_params.do_post_filtering)
    [result, new_number_of_steps] = filter_steps(result, new_number_of_steps, step_filter_params);
  end;
end

% ------------------------------------------------------------------------------

function [ result, ...
           centered_positions_forward ] ...
  = step_forward_lateral_decomposition( ...
      result, ... % A struct for aggregating results
      steps, ... % Struct: stride segmentation and relevant data
      position, ... % Matrix of Doubles: (n_frames x 3)
      DIRECTION_STEPS, ... % Int
      EXTRA_FRWD_CORRECTION, ... % Int
      PLOT_DETAILS ... % Int
    )

  % Decompose each step into forward and lateral directions, where the
  % total positional displacement attained over "nearby" steps is taken
  % to be the forward direction.

  % Unpack arguments
  step_start = steps.step_start;
  step_end   = steps.step_end;

  % Pre-allocate
  positions_forward = zeros(steps.number_of_steps, steps.max_step_nIndices);
  positions_lateral = zeros(steps.number_of_steps, steps.max_step_nIndices);

  for (i = 1:steps.number_of_steps)
    step_indices = step_start(i):step_end(i);
    step_nIndices = length(step_indices);

    if(DIRECTION_STEPS)
      % Define the local heading of a step to be the direction of total
      % displacement attained over the sequence of steps within
      % `DIRECTION_STEPS` indices from the current step.

      nearby_steps_indices = getNearbySteps(steps, DIRECTION_STEPS, i);

      % TODO: Replace the linear fit with something better. Maybe use covariance?
      nearby_steps = step_start(nearby_steps_indices);
      x = position(nearby_steps, 1);
      y = position(nearby_steps, 2);
      pol = polyfit(x, y, 1);
      mean_step_direction = atan2(polyval(pol, x(end)) - polyval(pol, x(1)), ...
                                  x(end) - x(1));
    else
      % When `DIRECTION_STEPS` is zero, use the direction of the step as a
      % fallback. We currently do this only because the code following this
      % conditional requires `mean_step_direction` to be defined.
      mean_step_direction = atan2( ...
        position(step_end(i), 2) - position(step_start(i), 2), ...
        position(step_end(i), 1) - position(step_start(i), 1));
    end;

    % Express the step trajectory in a reference frame where the x-axis points
    % in the direction of the local heading.
    [ positions_forward(i, 1:step_nIndices), ...
      positions_lateral(i, 1:step_nIndices) ] = ...
      rotate_angle( position(step_indices, 1), ...
                    position(step_indices, 2), ...
                    -mean_step_direction );

    % To fill the rest of the array, repeat the last value.
    positions_lateral(i, step_nIndices+1:end) = positions_lateral(i, step_nIndices);
    positions_forward(i, step_nIndices+1:end) = positions_forward(i, step_nIndices);

    % Make the trajectory of lateral dispalcement continuous over all steps
    % by translating the steps so that their endpoints coincide.
    % TODO: Why do we do this for `positions_lateral` but not `positions_forward`?
    if i > 1
        positions_lateral(i, :) = positions_lateral(i, :) - positions_lateral(i, 1) + positions_lateral(i-1, end);
    else
        % Translate the initial position to the origin
        positions_lateral(i, :) = positions_lateral(i, :) - positions_lateral(i, 1);
    end;
  end;

  if EXTRA_FRWD_CORRECTION
    [positions_forward, positions_lateral] = ...
      extra_straightening_corrections(positions_forward, positions_lateral, PLOT_DETAILS);
  end

  % Translate the step trajectories so that they each begin at the origin.
  centered_positions_forward = ...
    positions_forward - positions_forward(:, 1) * ones(1, steps.max_step_nIndices);
  centered_positions_lateral = ...
    positions_lateral - positions_lateral(:, 1) * ones(1, steps.max_step_nIndices);

  % Append to results
  result.abs_ltrl = positions_lateral';
  result.abs_frwd = positions_forward';
  result.frwd = centered_positions_forward';
  result.ltrl = centered_positions_lateral';
end


function [ frwd_straighten, ...
           ltrl_straighten ] ...
  = extra_straightening_corrections( ...
      positions_forward, ...
      positions_lateral, ...
      PLOT_DETAILS ...
    )

  % Apply heavy-handed step-straightening corrections.
  % Note: This code probably operates under the assumption that the
  % trajectory is a recording of straight-line walking.

  % Estimate the overall direction of the entire trajectory
  positions_lateral_end = positions_lateral(: , end);
  positions_forward_end = positions_forward(: , end);
  pol = polyfit(positions_forward_end, positions_lateral_end, 1);
  overall_trajectory_direction = atan(pol(1));

  % Find the mean lateral step displacement
  [frwd_endr, ltrl_endr] = rotate_angle(positions_forward_end, positions_lateral_end, -overall_trajectory_direction);
  center_ltrl_end = mean(ltrl_endr);
  ltrl_endr = ltrl_endr - center_ltrl_end; % TODO: why do we center the lateral step displacement?

  % Decompose the trajectory into forward and lateral directions, where the
  % overall direction is taken to be the forward direction
  [frwd_straighten, ltrl_straighten] = rotate_angle(positions_forward, positions_lateral, -overall_trajectory_direction);
  ltrl_straighten = ltrl_straighten - center_ltrl_end; % Remove the mean lateral displacement

  if(PLOT_DETAILS)
    frwd_pol = (min(positions_forward_end) : max(positions_forward_end));
    ltrl_pol = polyval(pol, frwd_pol);

    figure(ANG_FIG);
    hold on;
    plot(foot_heading,'k');
    xlabel('Step #');ylabel('Ang [rad]');
    legend('Org','Linear Fit','Line-Fit Correction','Piecewise Correction');
    hold off;

    figure(PATH_FIG)
    hold on;
    plot(positions_forward(:,end), positions_lateral(:,end),'b');
    xlabel('X [m]');ylabel('Y [m]');
    plot(positions_forward_end, positions_lateral_end, '*b');
    plot(frwd_pol, ltrl_pol, 'r');
    hold off;
    legend('Line-Fit Correction','Piecewise Correction', '', '');

    [frwd_pol_rot, ltrl_pol_rot] = rotate_angle(frwd_pol, ltrl_pol, -overall_trajectory_direction);
    ltrl_pol_rot = ltrl_pol_rot - mean(ltrl_pol_rot);

    hold on;
    plot(frwd_pol_rot, ltrl_pol_rot, 'g');
    plot(frwd_endr, ltrl_endr, 'g*');
    plot(frwd_straighten(:,end),ltrl_straighten(:,end),'g');
    xlabel('X [m]');ylabel('Y [m]');
    legend('Line-Fit Correction','Piecewise Correction', '', '', 'Best correction');
    hold off;
  end;
end


function result = step_elevation_and_pitch(result, steps, positions, euler_angles)
  % Extract trajectories of elevation and pitch for each step

  % Unpack arguments
  step_start = steps.step_start;
  step_end   = steps.step_end;

  % Pre-allocate
  elevation = zeros(steps.number_of_steps, steps.max_step_nIndices);
  theta     = zeros(steps.number_of_steps, steps.max_step_nIndices);

  for i = 1:steps.number_of_steps
    step_indices = step_start(i):step_end(i);
    step_nIndices = length(step_indices);

    elevation(i, 1:step_nIndices) = positions(step_indices, 3);
    elevation(i, step_nIndices+1:end) = elevation(i, step_nIndices); % Fill array by repeating last element

    theta(i, 1:step_nIndices) = euler_angles(step_indices, 2);
    theta(i, step_nIndices+1:end) = theta(i, step_nIndices); % Fill array by repeating last element
  end

  % Translate the step trajectories so that they each begin at the origin
  centered_elev = elevation - elevation(:, 1) * ones(1, steps.max_step_nIndices);

  % Note: This line was commented out. TODO: Why?
  %theta = theta - theta(:,1)*ones(1,steps.max_step_nIndices);

  % Append to results
  result.elev = centered_elev';
  result.theta = theta';
end


function result = step_speeds( ...
  result, ...
  steps, ...
  centered_positions_forward, ... % Step trajectories, each beginning at the origin, projected onto a "forward" direction
  sampling_period ... % Double, the IMU sampling period
  )

  % Unpack arguments
  step_start = steps.step_start;
  step_end   = steps.step_end;

  step_lengths_forward = centered_positions_forward(:,end)';
  step_nSamples = (step_end - step_start)';
  step_durations = step_nSamples * sampling_period;
  step_speeds_forward = step_lengths_forward ./ step_durations;

  % TODO: What is the reasoning behind this compensation?
  pol = polyfit(step_speeds_forward, step_lengths_forward, 1);
  step_length_fit = polyval(pol, step_speeds_forward);
  step_speeds_forward_compensated = step_lengths_forward - step_length_fit;

  % Append to results
  result.step_samples = step_nSamples;
  result.time = step_durations;
  result.frwd_speed_compensated = step_speeds_forward_compensated;
  result.frwd_speed = step_speeds_forward;
end


function result = step_heading(result, steps, euler_angles, PLOT_DETAILS, DIRECTION_STEPS)
  % Define the heading of a step by the yaw of the foot at the end of the
  % step.

  % Attempt to correct for drift in the yaw by subtracting out a linear fit.
  % This correction is only applicable to certain trajectories, such as short
  % and straight bouts of walking.

  % Unpack arguments
  step_start = steps.step_start;
  step_end   = steps.step_end;

  % Eliminate discontinuities
  walk_foot_heading = unwrap(euler_angles(step_end, 3));

  % Remove linear trends in yaw
  x = (1:length(walk_foot_heading))';
  y = walk_foot_heading;
  pol = polyfit(x, y, 1);
  heading_correction = polyval(pol, x);
  corrected_heading = y - heading_correction;

  if PLOT_DETAILS
    ANG_FIG = figure;
    plot(walk_foot_heading-pol(2));
    hold on; grid on;
    plot(heading_correction-pol(2),'r');
    plot(y-heading_correction,'g');
  end;

  if DIRECTION_STEPS > 0
    % Remove _local_ linear trends in yaw
    for i = 1:steps.number_of_steps
      step_indices = step_start(i):step_end(i);
      step_nIndices = length(step_indices);

      nearby_steps_indices = getNearbySteps(steps, DIRECTION_STEPS, i);

      % Find a local heading correction
      % TODO: What is the meaning of this computation?
      x = (nearby_steps_indices)';
      y = walk_foot_heading(nearby_steps_indices);
      pol = polyfit(x, y, 1);
      current_index = find(x == i);
      heading_correction = polyval(pol, current_index);
      corrected_heading(i) = y(current_index) - heading_correction;
    end;
  end;

  % Append to results
  result.foot_heading = corrected_heading;
end


function result = step_forward_lateral_decomposition_swing(result, steps, positions)
  % Decompose the step into forward and lateral directions, where the
  % positional displacement of the step is taken to be the forward
  % direction.

  % Unpack arguments
  step_start = steps.step_start;
  step_end   = steps.step_end;

  % Pre-allocate
  ltrl_swing = zeros(steps.number_of_steps, steps.max_step_nIndices);
  frwd_swing = zeros(steps.number_of_steps, steps.max_step_nIndices);

  for i = 1:steps.number_of_steps
    step_indices = step_start(i):step_end(i);
    step_nIndices = length(step_indices);

    step_direction = atan2( ...
      positions(step_end(i), 2) - positions(step_start(i), 2), ...
      positions(step_end(i), 1) - positions(step_start(i), 1));

    [ frwd_swing(i, 1:step_nIndices), ...
      ltrl_swing(i, 1:step_nIndices) ] = ...
      rotate_angle( positions(step_indices, 1), ...
                    positions(step_indices, 2), ...
                    -step_direction );

    % Repeat the last value to fill the array
    frwd_swing(i, step_nIndices+1:end) = frwd_swing(i, step_nIndices);
    ltrl_swing(i, step_nIndices+1:end) = ltrl_swing(i, step_nIndices);
  end

  % Translate steps so that they begin at the origin
  frwd_swing = frwd_swing - frwd_swing(:, 1) * ones(1, steps.max_step_nIndices);
  ltrl_swing = ltrl_swing - ltrl_swing(:, 1) * ones(1, steps.max_step_nIndices);

  % Append to results
  result.frwd_swing = frwd_swing';
  result.ltrl_swing = ltrl_swing';
end


% ------------------------------------------------------------------------------
% Helper functions

function nearby_steps_indices = getNearbySteps(steps, DIRECTION_STEPS, stepNo)
  % Get the indices of the nearby steps, minding the endpoints
  if (DIRECTION_STEPS < stepNo && stepNo < steps.number_of_steps - DIRECTION_STEPS)
      nearby_steps_indices = (stepNo - DIRECTION_STEPS:stepNo + DIRECTION_STEPS);
  elseif (stepNo <= DIRECTION_STEPS)
      nearby_steps_indices = (1:stepNo + DIRECTION_STEPS);
  else % stepNo >= steps.number_of_steps - DIRECTION_STEPS
      nearby_steps_indices = (stepNo - DIRECTION_STEPS:steps.number_of_steps);
  end;
end
