classdef TiltCorrectionKalmanFilterParams
  properties
    apply_tilt_correction; % Boolean

    % Will be multiplied by the sampling period.
    % Note: The Kalman filter fudges the variances, so I'm not sure what the
    % units are.
    gyroscope_process_variance; % Double

    % Will be multiplied by the sampling period (though I'm not sure why this
    % should be the case).
    % Note: The Kalman filter fudges the variances, so I'm not sure what the
    % units are.
    accelerometer_measurement_variance; % Double
  end

  methods
    % Default values
    function obj = TiltCorrectionKalmanFilterParams(obj)
      obj.apply_tilt_correction              = true;
      obj.gyroscope_process_variance         = 1.5e-5;
      obj.accelerometer_measurement_variance = 1.5e-1;
    end
  end
end
