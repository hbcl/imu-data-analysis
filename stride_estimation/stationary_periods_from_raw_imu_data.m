function [ stationary_frame_indices,     ... % Row vector of Integers
           angular_velocity_magnitudes ] ... % Row vector of Doubles (n_frames)
  = stationary_periods_from_raw_imu_data( ...
      sampling_period,               ... % Double, the IMU sampling period
      stationary_period_params,      ... % StationaryPeriodParams
      imu_local_angular_velocities,  ... % Matrix of Doubles (n_frames x 3)
      imu_local_linear_accelerations ... % Matrix of Doubles (n_frames x 3)
      )
  % Find frames with both a small angular velocity and a small linear
  % acceleration.

  % The observed acceleration of a stationary IMU will have magnitude equal to
  % that of gravity; however, we do not know (with certainty) the expected
  % direction of this acceleration since the acceleration is represented in the
  % yet unknown local local reference frame of the IMU. Our current approach is
  % to use the _difference in magnitude_ between the observed acceleration and
  % the acceleration due to gravity as an approximate indicator of low linear
  % acceleration.

  % Rename parameters
  max_imu_ang_vel = stationary_period_params.foot_fall_angular_velocity_maximum;
  max_imu_lin_acc = stationary_period_params.foot_fall_linear_acceleration_maximum;

  % Constants
  GRAVITY = 9.80297286843;

  angular_velocity_magnitudes = ...
    sum(imu_local_angular_velocities.^2, 2)' .^.5*180/pi/sampling_period; % deg / second

  linear_acceleration_magnitudes_minus_gravity = ...
    sum(imu_local_linear_accelerations.^2, 2)' .^.5 - GRAVITY;
  
  stationary_frame_indices = find( ...
    angular_velocity_magnitudes < max_imu_ang_vel & ...
    abs(linear_acceleration_magnitudes_minus_gravity) < max_imu_lin_acc ...
    );
end % function
