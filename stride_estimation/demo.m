%% A demonstration of the two-foot data processing code
% Run the two-foot analysis with default parameters on sample data.
% This demo requires the signal and statistics toolboxes.

%% Preamble
% Load the raw IMU data.

% Use default parameters

params = IMUAnalysisParameters();

% Load IMU data

[Wb_left, Ab_left, sampling_period_left] = ...
  getdata_ms('test_data/data_left_memsense.dat');

[Wb_right, Ab_right, sampling_period_right] = ...
  getdata_ms('test_data/data_right_memsense.dat');

% Sanity check

if sampling_period_left ~= sampling_period_right
  error('The sampling periods of the data from the left and right feet do not match.');
end
sampling_period = sampling_period_left;

% Preprocess data

SECTION = [190, 280]; % Hard-coded specification of the time interval to be processed (units: seconds).
[Wb_left,  Ab_left]  = getdata(Wb_left,  Ab_left,  sampling_period, SECTION);
[Wb_right, Ab_right] = getdata(Wb_right, Ab_right, sampling_period, SECTION);

%% Integration
% Transform local IMU data into a global frame. Attempt to correct for
% integration error using assumptions specific to IMUs placed on the feet.

[left_walk_info, right_walk_info] = ...
  integrate_two_foot_imus( ...
    Wb_left, Ab_left, Wb_right, Ab_right, ...
    sampling_period, ...
    params.stationary_period_params, ...
    params.foot_fall_detection_params, ...
    params.foot_fall_opposite_velocity_detection_params, ...
    params.tilt_correction_kalman_filter_params);

%% Stride segmentation
% Partition the data into steps.

strides_left  = stride_segmentation( ...
  left_walk_info,  sampling_period, params.step_filter_params);
strides_right = stride_segmentation( ...
  right_walk_info, sampling_period, params.step_filter_params);

plt_ltrl_frwd_strides(strides_left, params.plot_params);
plt_ltrl_frwd_strides(strides_right, params.plot_params);

plt_frwd_elev_strides(strides_left, params.plot_params);
plt_frwd_elev_strides(strides_right, params.plot_params);

plt_stride_var(strides_left, params.plot_params);
plt_stride_var(strides_right, params.plot_params);
