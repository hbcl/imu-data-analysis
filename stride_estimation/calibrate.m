function [W, A] = calibrate(static_interval, W, A)

% static_interval: 1x2 array of integers, representing the endpoints of an
% interval of indices. The data within this interval should be static; that
% is, there should be no movement.

start_index = static_interval(1);
end_index   = static_interval(2);

% Compensate for bias in the gyroscope. Assuming that the true angular
% velocity during the static period is 0, subtract from the angular
% velocity data the mean angular velocity observed during the static
% period.
W = W-ones(size(W,1),1)*mean(W(start_index:end_index, :));

% Calibrate the accelerometer. Assuming that the true acceleration during
% the static period is equal to the acceleration due to gravity, normalize
% the acceleration data so that its magnitude matches that of gravity
% during the static period.
GRAVITY = 9.8; %9.80297286843;
static_acceleration_magnitudes = ...
    sum((A(start_index:end_index, :).^2)').^.5;
observed_gravity_magnitude = mean(static_acceleration_magnitudes);
A = A*GRAVITY/observed_gravity_magnitude;
end