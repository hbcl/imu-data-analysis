# Implementation of the program

January 24, 2020

The following informal diagram outlines the structure of the imu-data-analysis
code.

This diagram was originally prepared for a presentation in a lab meeting,
the goal of which was to give a high-level overview of the code.
You should not expect to gain a precise understanding of the code from this
document alone.

A partial explanation of the diagram follows after the diagram.

![](diagrams/imu-data-analysis.svg)

## Interpretation of the diagram

### Legend

Large double-stroked boxes indicate conceptual groupings (e.g. "Single-foot
integration").

Ovals represent data and arrows represent dependencies between data.

Diamonds annotate data, labelling them as the inputs or outputs of
conceptual groupings. Boxes annotate data dependences, describing the function
relating the data.

The types of the data are sometimes included in an informal way.
For example, `: time -> R^3` is meant to be read as
"has the type of a function from time to triples of real numbers",
by which I mean that the data is a time series of 3D vectors.

### Notes

#### Single-foot integration

This part of the program transforms and filters raw data from a single IMU.
Raw IMU data is reported using a "local" reference frame
and needs to be transformed into a "global" reference frame.

In order to compute this transformation,
we must compensate for errors in the angular velocities reported by the IMU,
which would otherwise accumulate as we integrate them over time.
Thus, the program implements a form of tilt correction,
the assumptions of which are as follows.
The program assumes that the IMU is attached to the foot of a walking person
so that there will be periods where the foot is planted on the ground and relatively still.
During such stationary periods, the program assumes further that,
the only force acting on the IMU is gravity, which is sensed by the accelerometer.
The estimated orientation of the IMU is then adjusted to be more consistent
with the accelerometer measurements through a Kalman filter-like process.

As with the angular velocity measurements,
we compensate for errors in the accelerations reported by the IMU
by using the same stationary periods.
Stationary periods are used to define boundaries dividing the recording into discrete footsteps.
Assuming that the foot begins and ends each step in a completely stationary state,
the total acceleration of the foot over a step should be zero.
Accordingly,
the program adjusts the accelerations so that the total acceleration over each
step is exactly zero;
this is called the "zero-velocity update".

#### Two-foot integration

The individually obtained single-foot integration results of the left and right feet
are combined and re-analyzed in the two-foot integration process.
The two-foot integration uses an alternate procedure to divide the recoridng into discrete footsteps.
This alternate procedure uses the (horizontal) velocity peaks of the opposite foot as step boundaries.
There is a certain assumption that justifies this, but I don't remember what it is
-- Art explained it to me once, so you can ask him.
This new set of step boundaries is then used to redo the zero-velocity updates,
starting from uncorrected acceleration data.

#### `stride_segmentation` and `get_steps`

The intention behind a lot of this code is unknown to me; we guess that much of
the code is probably ad-hoc and written for specific purposes but for some
reason was left in the main branch.
I decided not to try to understand, clean up, or rewrite the code, given that
such a task would be better executed by someone with a better understanding of
the walking experiments that generate the data on which this program operates.
As a result, my comments for this section will be brief.

The integration outputs are fed into these functions.
These functions do a lot of things and pack all the results into a big struct.
It looks like the most significant of these analyses is to decompose the steps
into forward and lateral directions.
This is done in two different ways, probably to accomodate a variety of
experimental assumptions and conditions.
There is also some code to use these results to filter out steps that look like
outliers.

#### Parameters

I have extracted a lot of hard-coded parameters from the code
and placed them into classes, which are then given as parameters to functions
that require them.
These classes have names ending in `Params`, and are all collected
in the class `IMUAnalysisParameters`.

This structure for the parameters -- a single class composed of other classes
composed of the parameters -- is expected by one of the users of this code
(the AutoOsman program). Furthermore, only numeric parameters can currently be
handled by AutoOsman.

#### Other

Lots of plots are generated all throughout these procedures,
many of which I do not understand.
I assume a lot of them are just for sanity-checking and debugging.
