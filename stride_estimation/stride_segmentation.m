% Author: Lauro Ojeda, 2011-2015
function stepData = stride_segmentation(walk_info, sampling_period, step_filter_params)

  FF = find(walk_info.foot_fall_frame_mask);

  swing_start = FF(1:end-1);
  swing_finish = FF(2:end);

  % Filter out long steps
  MAX_FF_INDICES = step_filter_params.max_step_duration / sampling_period;
  swing_time = diff(FF);
  too_long = find(swing_time > MAX_FF_INDICES);
  if(~isempty(too_long))
      disp(sprintf('Eliminted %d out of %d steps because of time duration is longer than %d',size(too_long,1), length(swing_start), MAX_FF_INDICES));
  end;
  swing_start(too_long) = [];
  swing_finish(too_long) = [];

  stepData = get_steps(swing_start, swing_finish, walk_info, sampling_period, step_filter_params);
end
