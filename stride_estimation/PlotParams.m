classdef PlotParams
  % Parameters used by some of the plots.

  properties
    % Whether or not to highlight the endpoints of the strides.
    show_endpoints; % Boolean

    % Whether or not to label strides with their stride numbers
    show_number; % Boolean

    % Plot a minimal
    quick_plot; % Boolean

    % For plotting elevations, whether or not to assume that the walking was
    % performed over a flat surface. If so, plot de-trended elevation data.
    assume_flat_surface; % Boolean
  end

  methods
    % Default values
    function obj = PlotParams(obj)
      obj.show_endpoints = true;
      obj.show_number = false;
      obj.quick_plot = false;
      obj.assume_flat_surface = true;
    end
  end
end
